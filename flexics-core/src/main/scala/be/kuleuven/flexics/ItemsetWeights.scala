package be.kuleuven.flexics

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.{BinaryDataset, Itemset}
import be.kuleuven.pmlib.labels.{BinaryLabel, QualityMeasure}
import be.kuleuven.weightgen.{BoundedWeightFunction, WeightFunction}

object SupportUtils {
  def supports(dataset: Dataset[Set[Int]], constraints: Seq[Constraint]): (Int, Int) = {
    val minsup = constraints.collectFirst { case ms: MinSupport => ms.minsup }.getOrElse(1)
    val maxsup = constraints.collectFirst {
      case ne: ExcludeEmptyItemset => itemSupports(dataset).max
      case ms: MaxSupport => ms.maxsup
    }.getOrElse(dataset.size)
    (maxsup, minsup)
  }

  def itemSupports(dataset: Dataset[Set[Int]]): Seq[Int] = dataset match {
    case bds: BinaryDataset => bds.itemCovers.map(_.size)
    case _ => dataset.records.view.foldLeft(Array.fill(dataset.attributes.size)(0)) { case (counts, t) =>
      t.values.foreach(i => counts(i) += 1)
      counts
    }.toSeq
  }
}

private final class Support(maxsup: Int, minsup: Double) extends BoundedWeightFunction[Itemset] {
  private val maxsup0 = maxsup.toDouble

  override def weight(itemset: Itemset): Double = itemset.size / maxsup0
  override def tiltBound: Double = maxsup0 / minsup

  override def toString = "Support"
}

final class Length(itemCount: Int) extends BoundedWeightFunction[Itemset] {
  private val itemCount0 = itemCount.toDouble

  override def weight(itemset: Itemset): Double = itemset.description.length / itemCount0
  override def tiltBound: Double = itemCount0

  override def toString = "Length"
}

private final class LogSupport(maxsup: Int, minsup: Double) extends BoundedWeightFunction[Itemset] {
  import scala.math.{log10, max}

  private val logMinsup = max(log10(minsup), 1)
  private val logMaxsup = log10(maxsup)

  override def weight(itemset: Itemset): Double = max(log10(itemset.size), 1) / logMaxsup
  override val tiltBound: Double = logMaxsup / logMinsup

  override def toString = "LogSupport"
}

trait WeightFunctionClass {
  def apply(dataset: Dataset[Set[Int]], constraints: Seq[Constraint]): WeightFunction[Itemset]
}

object Support extends WeightFunctionClass {
  override def apply(dataset: Dataset[Set[Int]], constraints: Seq[Constraint]): WeightFunction[Itemset] = {
    val (maxsup, minsup) = SupportUtils.supports(dataset, constraints)
    new Support(maxsup, minsup)
  }
}

object LogSupport extends WeightFunctionClass {
  override def apply(dataset: Dataset[Set[Int]], constraints: Seq[Constraint]): WeightFunction[Itemset] = {
    val (maxsup, minsup) = SupportUtils.supports(dataset, constraints)
    new LogSupport(maxsup, minsup)
  }
}

private final class Surprisingness(val itemFrequenices: Seq[Double],
                                   val datasetSize: Double,
                                   val tiltBound: Double) extends WeightFunction[Itemset] {
  override def weight(itemset: Itemset): Double = {
    val freq = itemset.size / datasetSize
    freq * freq * itemset.items.map(itemFrequenices).product
  }

  override def initialMinWeight: Double = 1.0
  override def theoreticalMaxWeight(observedMinWeight: Double): Double = 1.0

  override def toString = "Surprisingness"
}

object Surprisingness extends WeightFunctionClass {
  override def apply(dataset: Dataset[Set[Int]], constraints: Seq[Constraint]): WeightFunction[Itemset] = {
    val datasetSize = dataset.size.toDouble
    val itemFrequencies = SupportUtils.itemSupports(dataset).map(_ / datasetSize)

    val (maxsup, minsup) = SupportUtils.supports(dataset, constraints)
    val tiltBound = (maxsup.toDouble / minsup) *
      (itemFrequencies.max / itemFrequencies.filter { f =>
        val support = f * datasetSize
        minsup <= support && support <= maxsup
      }.product)

    new Surprisingness(itemFrequencies, datasetSize, tiltBound)
  }
}

abstract class SupervisedWeightFunction extends WeightFunctionClass {
  val measure: QualityMeasure[BinaryLabel]

  override final def apply(dataset: Dataset[Set[Int]], constraints: Seq[Constraint]): WeightFunction[Itemset] =
    dataset match {
      case lds: Dataset[Set[Int]] with BinaryLabel =>
        val state = Some(measure.computeState(lds))
        val tilt = tiltBound(lds, constraints)
        val norm = normalizationConstant(lds, constraints)
        new BoundedWeightFunction[Itemset] {
          override def weight(itemset: Itemset): Double = measure.score(itemset, lds, state) / norm
          override val tiltBound: Double = tilt
          override def toString = measure.toString
        }
      case _ => throw new IllegalArgumentException
    }

  protected def tiltBound(dataset: Dataset[Set[Int]] with BinaryLabel, constraints: Seq[Constraint]): Double

  protected def normalizationConstant(dataset: Dataset[Set[Int]] with BinaryLabel, constraints: Seq[Constraint]): Double

  override def toString = measure.toString
}

object Purity extends SupervisedWeightFunction {
  val measure = be.kuleuven.pmlib.labels.binary.Purity

  override protected def tiltBound(ds: Dataset[Set[Int]] with BinaryLabel, cs: Seq[Constraint]): Double = 2

  override protected def normalizationConstant(ds: Dataset[Set[Int]] with BinaryLabel, cs: Seq[Constraint]): Double = 1
}

