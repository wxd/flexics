package be.kuleuven

import be.kuleuven.pmlib.itemsets.Itemset

package object flexics {
  val WeightKey = "wg4ps.weight"

  def storeWeightInPlace(p: Itemset, w: Double): Itemset = {
    p.storeMetadata(WeightKey, w)
    p
  }

  val GetWeight: Itemset => Double = { _.getMetadata[Double](WeightKey).get }
}
