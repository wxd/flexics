package be.kuleuven.flexics

trait Constraint
trait Coverage extends Constraint
trait Occurrence extends Constraint
trait ExcludeEmptyItemset extends Constraint
trait MinSupport extends Constraint { val minsup: Int }
trait MaxSupport extends Constraint { val maxsup: Int }
trait MinLength extends Constraint { val minlen: Int }
trait MaxLength extends Constraint { val maxlen: Int }
trait Closed extends Constraint

