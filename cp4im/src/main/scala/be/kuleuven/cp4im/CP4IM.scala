package be.kuleuven.cp4im

import be.kuleuven.cp4im.jni.swig.{Fimcp_java, Searcher_java, fimcp_jni}
import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.pmlib.patterns.{Mask, MaskBuilder}
import be.kuleuven.weightgen.{ParityConstraint, Problem}
import be.kuleuven.flexics._
import org.scijava.nativelib.NativeLoader

final class CP4IMIterator(private val solver: Searcher_java,
                          private val dataset: Dataset[Set[Int]],
                          private val buildMask: Boolean) extends Iterator[Itemset] {
  private var nextItemset: Itemset = _

  def terminate(): Unit = solver.delete()

  override def hasNext: Boolean = {
    val hasSolution = solver.next()
    if (hasSolution) {
      nextItemset = extract()
    } else {
      nextItemset = null
      solver.delete()
    }

    hasSolution
  }

  override def next(): Itemset = nextItemset

  private def extract(): Itemset =
    Itemset(extractItems(), dataset, extractMask())

  private def extractItems(): Set[Int] =
    Iterator.iterate(-1) { prev => solver.next_item(prev) }.drop(1).takeWhile(_ >= 0).toSet

  private def extractMask(): Option[Mask] =
    if (buildMask) {
      val builder = new MaskBuilder(datasetSize = dataset.size)
      Iterator.iterate(-1) { prev => solver.next_transaction(prev) }
        .drop(1).takeWhile(_ >= 0)
        .foreach { t => builder.set(t) }
      Some(builder.build())
    } else {
      None
    }

}

final class CP4IM private(val dataset: Dataset[Set[Int]],
                          val constraints: Iterable[Constraint],
                          val buildMask: Boolean = true,
                          private val wrapper: Fimcp_java) extends Problem[Itemset] {
  override def varCount: Int = dataset.attributes.size
  override def toString = s"CP4IM(${dataset.name},${constraints.mkString("+")}"

  def solve() = new CP4IMIterator(wrapper.search(), dataset, buildMask)

  def copy(): CP4IM = new CP4IM(dataset, constraints, buildMask, wrapper.anotherone())

  def terminate(): Unit = wrapper.delete()

  def withExtraXOR(xor: ParityConstraint): CP4IM = {
    val bits = xor.coefficientSet.size
    val items = fimcp_jni.new_int_array(bits)
    xor.coefficientSet.iterator.zipWithIndex.foreach { case (v, i) => fimcp_jni.int_array_setitem(items, i, v) }

    val w = wrapper.withxor(bits, items, xor.parity)
    new CP4IM(dataset, constraints, this.buildMask, w)
  }

  def withExtraXORs(xors: Seq[ParityConstraint]): CP4IM = {
    val k = xors.size
    val sizes = fimcp_jni.new_int_array(k)
    val parities = fimcp_jni.new_bool_array(k)

    val totalBits = xors.view.map(_.coefficientSet.size).sum
    val coefficients = fimcp_jni.new_int_array(totalBits)

    var cap = 0 // Coefficients' array pointer
    xors.view.zipWithIndex.foreach { case (c, ci) =>
      val size = c.coefficientSet.size
      fimcp_jni.int_array_setitem(sizes, ci, size)
      fimcp_jni.bool_array_setitem(parities, ci, c.parity)

      c.coefficientSet.view.zipWithIndex.foreach { case (item, j) =>
        fimcp_jni.int_array_setitem(coefficients, cap + j, item)
      }

      cap += size
    }

    val w = wrapper.withxors(k, sizes, coefficients, parities)
    new CP4IM(dataset, constraints, this.buildMask, w)
  }
}

object CP4IM {
  NativeLoader.loadLibrary("cp4im")

  def apply(dataset: Dataset[Set[Int]],
            constraints: Iterable[Constraint],
            buildMask: Boolean = true): CP4IM = {
    val items = dataset.attributes.size
    val transactions = dataset.size

    val ds = datasetToArray(dataset)
    val wrapper = new Fimcp_java()
    wrapper.coverage(ds, items, transactions)

    constraints.foreach {
      case ms: be.kuleuven.flexics.MinSupport =>
        wrapper.minfreq(ds, items, transactions, ms.minsup)
      case _: be.kuleuven.flexics.Closed => wrapper.closed(ds, items, transactions)
      case mnl: be.kuleuven.flexics.MinLength => wrapper.minlen(mnl.minlen)
      case mxl: be.kuleuven.flexics.MaxLength => wrapper.maxlen(mxl.maxlen)
    }

    wrapper.init()
    fimcp_jni.delete_bool_array(ds)
    new CP4IM(dataset, constraints, buildMask, wrapper)
  }

  private def datasetToArray(dataset: Dataset[Set[Int]]) = {
    val items = dataset.attributes.size
    val transactions = dataset.size

    val ds = fimcp_jni.new_bool_array(items * transactions)
    dataset.records.foreach { t =>
      (0 until items).foreach { i =>
        fimcp_jni.bool_array_setitem(ds, t.index * items + i, t.values contains i)
      }
    }
    ds
  }
}
