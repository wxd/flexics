package be.kuleuven.cp4im

import be.kuleuven.pmlib.itemsets.{CP4IMData, CP4IMParameters}

object Main extends App {
  private val path = "/Users/vladimir/Documents/phd/code/wg4ps++/fimcp-2.7/src/vote.txt"
  private val ds = CP4IMData.load(path, CP4IMParameters(dropLabel = true))

  for (_ <- 1 to 20; buildMask <- Array(true, false)) {
    val problem = CP4IM(ds, List(MinSupport(40), Closed, MinLength(5)), buildMask)

    var n = 0
    val start = System.nanoTime()
    problem.solve().zipWithIndex.foreach { case (p, i) => n += 1 }
    val elapsed = (System.nanoTime() - start) / 1000000000.0

    println(buildMask, n, elapsed)
  }
}
