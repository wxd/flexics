package be.kuleuven.cp4im.jni;

import be.kuleuven.cp4im.jni.swig.Fimcp_java;
import be.kuleuven.cp4im.jni.swig.SWIGTYPE_p_bool;
import be.kuleuven.cp4im.jni.swig.Searcher_java;
import be.kuleuven.cp4im.jni.swig.fimcp_jni;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Main {
    private static final int ITEMS = 48;
    private static final int TRANSACTIONS = 435;

    public static void main(String[] args) {
        try {
            System.loadLibrary("cp4im");
            final SWIGTYPE_p_bool data = fimcp_jni.new_bool_array(ITEMS * TRANSACTIONS);

            final BufferedReader reader = new BufferedReader(new FileReader(new File("/Users/vladimir/Documents/phd/code/wg4ps++/fimcp-2.7/src/vote.txt")));
            String line;
            do {
                line = reader.readLine();
            } while (!line.equals("@data"));
            reader.readLine(); // skip an empty line

            int t = 0;
            while ((line = reader.readLine()) != null) {
                final String[] items = line.split(" ");
                for (int i = 0; i < items.length - 1; i++) {
                    final int item = Integer.parseInt(items[i]);
                    fimcp_jni.bool_array_setitem(data, t * ITEMS + item, true);
                }

                t++;
            }
            System.out.println(t);

            final Fimcp_java f = new Fimcp_java();
            System.out.println(f);
            f.coverage(data, ITEMS, TRANSACTIONS);
            f.minfreq(data, ITEMS, TRANSACTIONS, 40);
            f.closed(data, ITEMS, TRANSACTIONS);
            f.minlen(5);

            fimcp_jni.delete_bool_array(data);

            final boolean initResult = f.init();
            System.out.println(initResult);

            if (initResult) {
                final Searcher_java search = f.search();
                int n = 0;
                final long startTime = System.nanoTime();
                while (search.next()) {
                    System.out.println();
                    for (int i = 0; i < ITEMS; i++) {
                        if (search.item_in_solution(i))
                            System.out.print(i + " ");
                    }
                    System.out.println();

                    n++;
                }
                final long finishTime = System.nanoTime();
                System.out.println();
                System.out.printf("%d solutions in %.3fms\n", n, (finishTime - startTime) / 1000000.0);

                search.delete();
            }

            f.delete();
        } catch (UnsatisfiedLinkError ule) {
            ule.printStackTrace();
        } catch (Exception e) {
            System.out.println("BUE");
        }
    }
}
