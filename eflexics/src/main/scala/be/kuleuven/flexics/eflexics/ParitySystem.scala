package be.kuleuven.flexics.eflexics

import be.kuleuven.m4ri.DenseMatrixGF2
import be.kuleuven.weightgen.ParityConstraint
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

sealed trait ParitySystem {
  def copy(): ParitySystem
  def free(): Unit
  def unsetItem(i: Int): Unit
  def update(delta: ItemVariablesDelta): Unit
  def echelonise(): Unit
  def updateThenPropagate(itemVars: ItemVariables): Boolean
  def isValidAssignment(): Boolean
  def isValidExtension(i: Int): Boolean
  def isValidExtension(i1: Int, i2: Int): Boolean
}

object EmptyParitySystem extends ParitySystem {
  @inline override def copy(): ParitySystem = this
  @inline override def free(): Unit = ()
  @inline override def unsetItem(i: Int): Unit = ()
  @inline override def update(delta: ItemVariablesDelta): Unit = ()
  @inline override def echelonise(): Unit = ()
  @inline override def updateThenPropagate(itemVars: ItemVariables): Boolean = true
  @inline override def isValidAssignment(): Boolean = true
  @inline override def isValidExtension(i: Int): Boolean = true
  @inline override def isValidExtension(i1: Int, i2: Int): Boolean = true

  override def toString = "[empty]"
}

object ParitySystem {
  def apply(xors: Seq[ParityConstraint], n: Int) =
    if (xors.nonEmpty) {
      val m = new DenseMatrixGF2(xors.size, n + 1)
      xors.view.zipWithIndex.foreach { case (xor, r) =>
        m.set(r, n, xor.parity)
        xor.coefficientSet.foreach { i => m.set(r, i, true) }
      }

      new ParitySystemImpl(m, n, bottommostRow = xors.size - 1, leftmostColumn = 0)
    } else {
      EmptyParitySystem
    }
}

private[eflexics] final class ParitySystemImpl(private val matrix0: DenseMatrixGF2,
                                               private val n: Int,
                                               private var bottommostRow: Int,
                                               private var leftmostColumn: Int) extends ParitySystem with ParitySystemLogging {
  override protected val logger = Logger(LoggerFactory.getLogger("ParitySystem"))

  override def copy() = new ParitySystemImpl(this.matrix0.copy(), this.n, this.bottommostRow, this.leftmostColumn)

  override def free(): Unit = { matrix0.free() }

  private val rowCount      = matrix0.getRows
  private val columnCount   = n + 1
  private val rowIndices    = 0 until rowCount
  private val columnIndices = 0 until columnCount
  private val itemIndices   = columnIndices.dropRight(1)
  private val parityColumn  = n

  @inline private def activeRowIndices    = rowIndices.take(bottommostRow + 1)
  @inline private def activeColumnIndices = columnIndices.drop(leftmostColumn)
  @inline private def activeItemIndices   = activeColumnIndices.dropRight(1)

  override def unsetItem(i: Int): Unit =
    if (i >= leftmostColumn) {
      activeRowIndices.foreach(r => matrix0.set(r, i, false))
    }

  private def get(r: Int, c: Int) = {
    if (r < 0 || r >= rowCount || c < 0 || c >= columnCount)
      throw new RuntimeException(s"Cannot access ($r,$c) in ${rowCount}x$columnCount")
    matrix0.get(r, c)
  }
  private def parity(row: Int) = get(row, parityColumn)

  private def rowBits(r: Int): Seq[Int] = activeColumnIndices.view.filter(get(r, _))
  private def rowItemBits(r: Int): Seq[Int] = activeItemIndices.view.filter(get(r, _))

  override def update(delta: ItemVariablesDelta): Unit =
    activeRowIndices.foreach(updateRow(_, delta))

  override def echelonise(): Unit =
    if (bottommostRow >= 0) {
      if (bottommostRow < rowCount - 1 || leftmostColumn != 0)
        matrix0.echelonizeUpperRightCorner(bottommostRow, leftmostColumn, true)
      else
        matrix0.echelonize(true)
    }

  private def isRowConsistent(r: Int): Boolean = rowBits(r).headOption.forall(_ < parityColumn)
  private def isConsistent: Boolean = activeRowIndices.forall(isRowConsistent)

  private def updateRow(r: Int, delta: ItemVariablesDelta): Unit = {
    val existSetVariables = !delta.ones.isEmpty
    val parity = if (existSetVariables) get(r, parityColumn) else true
    var updatedParity = parity
    activeColumnIndices.foreach { c =>
      if (delta.hasBeenPruned(c)) {
        matrix0.set(r, c, false)
      } else if (existSetVariables && delta.hasBeenSet(c) && get(r, c)) {
        matrix0.set(r, c, false)
        updatedParity = !updatedParity
      }
    }

    if (parity != updatedParity)
      matrix0.set(r, parityColumn, updatedParity)
  }

  /** Updates the matrix with the delta, resets it, performs GE, and propagates variables.
    *
    * Assumes that delta contains variable assignments stemming from search and that
    * the matrix has been previously echelonised.
    */
  override def updateThenPropagate(itemVars: ItemVariables): Boolean = {
    if (bottommostRow == 0) {
      logSkippingPropagation()
      return true
    }

    val delta = itemVars.delta
    traceMatrix(this, delta, "Before update")

    activeRowIndices.foreach(updateRow(_, delta))
    traceMatrix(this, delta, "After update")

    if (!isConsistent) {
      logInconsistency()
      return false
    }

    if (bottommostRow >= 0)
      leftmostColumn = activeRowIndices.view.map { r => rowBits(r).headOption.getOrElse(columnCount) }.min

    echelonise()
    traceMatrix(this, delta, "Echelonised")

    delta.reset()

    var firstNonEmptyColumn = n - 1
    var lastNonEmptyRow = bottommostRow
    def pushEmptyRowDown(er: Int): Unit = {
      if (er < lastNonEmptyRow)
        matrix0.swapRows(er, lastNonEmptyRow)
      lastNonEmptyRow -= 1
    }
    activeRowIndices.reverse.foreach { r =>
      updateRow(r, delta)

      val parityBit = get(r, parityColumn)
      rowItemBits(r) match {
        case Seq(c) if !parityBit =>
          matrix0.set(r, c, false)
          itemVars.prune(c)

          pushEmptyRowDown(r)
          logPropagation(r, c, false)
        case Seq(c) if parityBit =>
          matrix0.set(r, parityColumn, false)
          matrix0.set(r, c, false)
          itemVars.set(c)

          pushEmptyRowDown(r)
          logPropagation(r, c, true)
        case Seq() if parityBit =>
          logInconsistency(r)
          return false
        case Seq() if !parityBit =>
          pushEmptyRowDown(r)
        case twoOrMore =>
          firstNonEmptyColumn = scala.math.min(twoOrMore.head, firstNonEmptyColumn)
      }
    }

    leftmostColumn = firstNonEmptyColumn
    bottommostRow = lastNonEmptyRow

    debugMatrix(this, delta, "After propagation")

    true
  }

  override def isValidAssignment(): Boolean =
    activeRowIndices.view.forall(r => !parity(r))

  override def isValidExtension(i: Int): Boolean =
    activeRowIndices.forall(r => if (i >= leftmostColumn) get(r, i) == parity(r) else !parity(r))

  override def isValidExtension(i1: Int, i2: Int): Boolean =
    if (i1 >= leftmostColumn && i2 >= leftmostColumn) {
      activeRowIndices.forall { r =>
        val b1 = get(r, i1)
        val b2 = get(r, i2)
        if (parity(r)) b1 != b2 else b1 == b2
      }
    } else if (i1 >= leftmostColumn) {
      isValidExtension(i1)
    } else if (i2 >= leftmostColumn) {
      isValidExtension(i2)
    } else {
      true
    }

  override def toString = {
    def rowString(r: Int) =
      itemIndices.view.map(c => if (get(r, c)) '1' else '_').mkString + '|' + (if (parity(r)) '1' else '0')

    s"""${matrix0.getRows}x${matrix0.getColumns}, COL0=ITEM$leftmostColumn
       |${rowIndices.view.map(rowString).mkString("\n")}""".stripMargin
  }
}

trait ParitySystemLogging {
  protected val logger: Logger

  protected def logSkippingPropagation() =
    logger.debug("Skipping GE propagation")

  protected def logInconsistency(row: Int) =
    logger.debug(s"Inconstistency in row $row")

  protected def logInconsistency() =
    logger.debug("Inconsistency after update")

  protected def logPropagation(row: Int, column: Int, value: Boolean) = {
    val action = if (value) "  set" else "unset"
    logger.trace(s"GE propagation $action column $column in row $row")
  }

  protected def debugMatrix(m: ParitySystem, delta: ItemVariablesDelta, reason: String) =
    logger.debug(s"$reason: $m\n$delta")

  protected def traceMatrix(m: ParitySystem, delta: ItemVariablesDelta, reason: String) =
    logger.trace(s"$reason: $m\n$delta")
}