package be.kuleuven.flexics.eflexics

import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.weightgen.leapfrogging.{LeapfroggingWeightGen, LeapfroggingWeightMC}
import be.kuleuven.weightgen.logging.{BasicSlf4jWeightGenLogger, BasicSlf4jWeightMcLogger}
import be.kuleuven.weightgen.sampling.BasicWeightGenWrapper
import be.kuleuven.weightgen.{WeightFunction, WeightMcConfiguration}
import be.kuleuven.flexics.eflexics.Eclat.EclatConfiguration

object EclatWeightMC extends LeapfroggingWeightMC[Itemset, EclatProblem] {
  private val oracle = EclatBoundedOracle(EclatConfiguration())

  override protected def defaultCounter(problem: EclatProblem) = oracle
  override protected val logger = new BasicSlf4jWeightMcLogger("EclatWeightMC")

  @inline
  override protected def iterationsBeforeLeapfrogging(p: EclatProblem, c: WeightFunction[Itemset], conf: WeightMcConfiguration) =
    3

  override def toString: String = "EclatWeightMC"
}

object EclatWeightGen extends LeapfroggingWeightGen[Itemset, EclatProblem] {
  private val oracle = EclatBoundedOracle(EclatConfiguration())

  override protected def defaultSolver(problem: EclatProblem) = oracle
  override protected val logger = new BasicSlf4jWeightGenLogger("EclatWeightGen")

  @inline
  override protected def initialExtraXors(p: EclatProblem, w: WeightFunction[Itemset], lt: Double, ht: Double, xors0: Int) =
    1

  override def toString: String = "EclatWeightGen"
}

object EFlexics extends BasicWeightGenWrapper[Itemset, EclatProblem] {
  override protected val counter = EclatWeightMC
  override protected val sampler = EclatWeightGen

  override def toString: String = "EFlexics"
}
