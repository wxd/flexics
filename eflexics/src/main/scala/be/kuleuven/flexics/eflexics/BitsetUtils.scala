package be.kuleuven.flexics.eflexics

import java.util

object BitsetUtils {
  def emptyBitset(n: Int): util.BitSet =
    new util.BitSet(n)

  def fullBitset(n: Int): util.BitSet = {
    val bs = emptyBitset(n)
    bs.set(0, n, true)
    bs
  }

  def clone(bs: util.BitSet): util.BitSet =
    bs.clone().asInstanceOf[util.BitSet]

  def setBits(bs: util.BitSet, n: Int): Iterable[Int] =
    (0 until n).view.filter(bs.get)
}
