package be.kuleuven.flexics

import java.util

package object eflexics {

  final class CandidateItem(val item: Int, val tids: util.BitSet, val support: Int) {
    def combine(otherTids: util.BitSet): CandidateItem = {
      val newTids: util.BitSet = tids.clone().asInstanceOf[util.BitSet]
      newTids.and(otherTids)

      new CandidateItem(item, newTids, newTids.cardinality())
    }
  }

  final class ItemVariablesDelta(val ones: util.BitSet, val zeros: util.BitSet, val n: Int) {
    def this(n: Int) =
      this(ones = BitsetUtils.emptyBitset(n), zeros = BitsetUtils.emptyBitset(n), n)

    def copy(): ItemVariablesDelta =
      new ItemVariablesDelta(BitsetUtils.clone(ones), BitsetUtils.clone(zeros), n)

    import BitsetUtils._

    def set() = setBits(ones, n)
    def pruned() = setBits(zeros, n)

    def isEmpty = ones.isEmpty && zeros.isEmpty

    def hasBeenSet(i: Int): Boolean = ones.get(i)
    def hasBeenPruned(i: Int): Boolean = zeros.get(i)
    def hasBeenFixed(i: Int): Boolean = hasBeenSet(i) || hasBeenPruned(i)

    def firstSetBit    = ones.nextSetBit(0)
    def firstPrunedBit = zeros.nextSetBit(0)
    def firstFixedBit  = scala.math.min(firstSetBit, firstPrunedBit)

    def reset(): Unit = {
      ones.clear()
      zeros.clear()
    }

    override def toString =
      (0 until n).view.map(i => if (ones.get(i)) '+' else if (zeros.get(i)) '-' else '_').mkString("")
  }

  final class ItemVariables(val ones: util.BitSet,
                            val zeros: util.BitSet,
                            val free: util.BitSet,
                            val delta: ItemVariablesDelta,
                            val size: Int) {
    def set(i: Int): Unit = {
      ones.set(i, true)
      free.set(i, false)
      delta.ones.set(i, true)
    }

    def prune(i: Int): Unit = {
      zeros.set(i, true)
      free.set(i, false)
      delta.zeros.set(i)
    }

    def setRange(i: Int, j: Int): Unit = {
      ones.set(i, j, true)
      free.set(i, j, false)
      delta.ones.set(i, j, true)
    }

    def pruneRange(i: Int, j: Int): Unit = {
      zeros.set(i, j, true)
      free.set(i, j, false)
      delta.zeros.set(i, j, true)
    }

    def copy(): ItemVariables =
      new ItemVariables(
        ones = BitsetUtils.clone(ones),
        zeros = BitsetUtils.clone(zeros),
        free = BitsetUtils.clone(free),
        delta.copy(),
        size
      )

    override def toString = (0 until size).view.map { i =>
      if (ones.get(i)) '1' else if (zeros.get(i)) '0' else '*'
    }.mkString("")
  }

  object ItemVariables {
    def apply(itemCount: Int): ItemVariables = {
      import BitsetUtils._
      new ItemVariables(
        ones  = emptyBitset(itemCount),
        zeros = emptyBitset(itemCount),
        free  = fullBitset(itemCount),
        delta = new ItemVariablesDelta(itemCount),
        itemCount
      )
    }
  }

  final case class PrefixWrapper(prefix: Array[Int], length: Int) {
    def set(item: Int): PrefixWrapper = {
      prefix(0) = item
      PrefixWrapper(prefix, 1)
    }

    def append(item: Int): PrefixWrapper = {
      prefix(length) = item
      PrefixWrapper(prefix, length + 1)
    }

    def append(items: Seq[Int]): PrefixWrapper = {
      items.view.zipWithIndex.foreach { case (item, i) => prefix(length + i) = item }
      PrefixWrapper(prefix, length + items.size)
    }

    def iterator = prefix.view(0, length).iterator

    override def toString = iterator.mkString(" ")
  }
}
