package be.kuleuven.flexics.eflexics

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.weightgen.ParityConstraint
import be.kuleuven.flexics.eflexics.Eclat.EclatConfiguration
import com.typesafe.scalalogging.Logger

trait EclatLogging {
  protected val logger: Logger

  protected final def logProblem(dataset: Dataset[Set[Int]], minsup: Int) = {
    logger.warn(s"${dataset.name}, minsup = $minsup")
    logger.info(s"${dataset.attributes.size} items, ${dataset.size} transactions")
  }

  protected final def logXors(xors: Seq[ParityConstraint], label: String) = {
    logger.warn(s"${xors.size} $label XORs")

    if (logger.underlying.isDebugEnabled())
      xors.foreach { xc => logger.debug(xc.toString) }
  }

  protected final def logConfig(config: EclatConfiguration) = {
    logger.debug(s"Config: do GE before projection = ${config.geBeforeProjection}")
    logger.debug(s"Config: do GE after projection  = ${config.geAfterProjection}")
    logger.debug(s"Config: during FF, check minsup at each step = ${config.checkUpdatedPrefixEarly}")
  }

  protected final def logFrequentItems(sortedFrequentItems: Seq[CandidateItem]) = {
    logger.warn(s"${sortedFrequentItems.size} frequent items")
    logger.info(s"Sorted frequent items: ${sortedFrequentItems.view.zipWithIndex.map { case (c, i) => s"${c.item}_$i" }.mkString(" ")}")
  }

  protected final def debugMatrix(matrix: ParitySystem, label: String) =
    logger.debug(s"[$label] $matrix")

  protected final def traceMatrix(matrix: ParitySystem, label: String) =
    logger.trace(s"[$label] $matrix")

  protected final def debugVarsMatrix(matrix: ParitySystem, vars: ItemVariables, label: String) =
    logger.debug(s"[$label] $matrix\n${vars.delta}\n$vars")

  protected final def traceVarsMatrix(matrix: ParitySystem, vars: ItemVariables, label: String) =
    logger.trace(s"[$label] $matrix\n${vars.delta}\n$vars")

  protected final def logPreparingEquivalenceClass(solution: PrefixWrapper,
                                                   candidateSuffixes: Seq[CandidateItem],
                                                   itemVars: ItemVariables,
                                                   gauss: ParitySystem) = {
    logger.debug(s"Preparing `$solution`: ${candidateSuffixes.size} candidate suffixes")
    logger.trace(s"Preparing `$solution`: $itemVars")
    logger.trace(gauss.toString)
  }

  protected final def logFrequentSuffixes(solution: PrefixWrapper,
                                          suffixes: Seq[CandidateItem],
                                          itemVars: ItemVariables,
                                          gauss: ParitySystem) = {
    val thereWasPruning = !itemVars.delta.zeros.isEmpty
    if (thereWasPruning)       logger.debug(s"Preparing `$solution`: ${suffixes.size} frequent suffixes")
    else if (suffixes.isEmpty) logger.debug(s"Preparing `$solution`: NO frequent suffixes")
    else                       logger.trace(s"Preparing `$solution`: ${suffixes.size} frequent suffixes")

    logger.trace(s"Preparing `$solution`: $itemVars")
    logger.trace(gauss.toString)
  }

  protected final def logFastForwardOutput(solution: PrefixWrapper) =
    logger.debug(s"Fast-forward and output: `$solution`")

  protected final def logGaussianElimination(solution: PrefixWrapper, label: String) =
    logger.trace(s"  Start GE $label `$solution`")

  protected final def logGaussianEliminationResult(solution: PrefixWrapper,
                                                   label: String,
                                                   suffixes: Seq[CandidateItem],
                                                   delta: ItemVariablesDelta) = {
    if (delta.isEmpty) logger.trace(s" Finish GE $label `$solution`: ${suffixes.size} suffixes")
    else               logger.debug(s" Finish GE $label `$solution`: ${suffixes.size} suffixes, $delta")
  }

  protected final def logFailureToFastForward(solution: PrefixWrapper, support: Int) =
    logger.debug(s"Failure GE `$solution`: support $support fell below `minsup`")

  protected final def logInconistency(solution: PrefixWrapper) =
    logger.debug(s"Failure GE `$solution`: inconsistent state")
}
