package be.kuleuven.flexics.eflexics

import java.util

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.pmlib.patterns.BitsetMask
import be.kuleuven.flexics.eflexics.Eclat.{EclatConfiguration, Listener}

final class EclatHelper(val config: EclatConfiguration,
                        private val dataset: Dataset[Set[Int]],
                        val minsup: Int,
                        private val itemNames: IndexedSeq[Int],
                        private val onItemset: Listener) {
  def output(prefix: PrefixWrapper, suffix: CandidateItem): Unit = {
    val items = (prefix.iterator ++ Iterator.single(suffix.item)).map(itemNames).toSet
    val mask = new BitsetMask(suffix.tids, dataset.size)
    val itemset = Itemset(items, dataset, Some(mask))

    if (config.storeItemsInSearchOrder)
      itemset.storeMetadata("items in search order", (prefix.iterator ++ Iterator.single(suffix.item)).mkString(" "))

    onItemset(itemset)
  }

  def maybeOutput(prefix: PrefixWrapper, suffix1: CandidateItem, suffix2: CandidateItem): Unit = {
    val jointTids = suffix1.tids
    jointTids.and(suffix2.tids)

    if (jointTids.cardinality() >= minsup) {
      val items = (prefix.iterator ++ Iterator(suffix1.item, suffix2.item)).map(itemNames).toSet
      val mask = new BitsetMask(jointTids, dataset.size)
      val itemset = Itemset(items, dataset, Some(mask))

      if (config.storeItemsInSearchOrder)
        itemset.storeMetadata(
          "items in search order",
          (prefix.iterator ++ Iterator(suffix1.item, suffix2.item)).mkString(" ")
        )

      onItemset(itemset)
    }
  }

  def output(prefix: PrefixWrapper, tids: util.BitSet): Unit = {
    val itemset = Itemset(prefix.iterator.map(itemNames).toSet, dataset, Some(new BitsetMask(tids, dataset.size)))

    if (config.storeItemsInSearchOrder)
      itemset.storeMetadata("items in search order", prefix.iterator.mkString(" "))

    onItemset(itemset)
  }
}
