package be.kuleuven.flexics.eflexics

import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.weightgen.ParityConstraint
import be.kuleuven.weightgen.oracle.{CallbackBasedBoundedOracle, CallbackBasedEnumerator, ListBufferBoundedOracle}
import be.kuleuven.flexics
import be.kuleuven.flexics.eflexics.Eclat.EclatConfiguration

final case class EclatBoundedOracle(eclatConfiguration: EclatConfiguration)
  extends CallbackBasedBoundedOracle[Itemset, EclatProblem] with ListBufferBoundedOracle[Itemset, EclatProblem] {

  override protected def storeSolutionWeight(solution: Itemset, weight: Double): Itemset =
    flexics.storeWeightInPlace(solution, weight) // Mutates `solution`!

  override protected def enumerator(p: EclatProblem,
                                    xors: IndexedSeq[ParityConstraint],
                                    newXor: Option[ParityConstraint]): CallbackBasedEnumerator[Itemset] = {
    new CallbackBasedEnumerator[Itemset] {
      override def enumerate(callback: (Itemset) => Unit): Unit =
        Eclat.minePreprocessed(p.dataset, p.threshold, p.candidateItems, xors, eclatConfiguration)(callback)
    }
  }
}
