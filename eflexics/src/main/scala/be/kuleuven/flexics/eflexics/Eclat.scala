package be.kuleuven.flexics.eflexics

import java.util

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.{BinaryDataset, Itemset}
import be.kuleuven.pmlib.patterns.BitsetMask
import be.kuleuven.weightgen.ParityConstraint
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.collection.immutable.IndexedSeq
import scala.util.control.Breaks._

object Eclat extends EclatLogging {
  type Listener = Itemset => Unit

  override protected val logger = Logger(LoggerFactory.getLogger("EclatXOR"))

  final case class EclatConfiguration(geBeforeProjection: Boolean = false,
                                      geAfterProjection: Boolean  = true,
                                      checkUpdatedPrefixEarly: Boolean = false,
                                      storeItemsInSearchOrder: Boolean = false,
                                      postSelectFrequentItems: Option[Seq[CandidateItem] => Seq[CandidateItem]] = None)

  def mine(dataset: Dataset[Set[Int]],
           minsup: Int,
           xors: Seq[ParityConstraint] = Seq.empty,
           config: EclatConfiguration  = EclatConfiguration())
          (onItemset: Listener): Unit =
    minePreprocessed(dataset, minsup, determineFrequentItems(dataset, minsup), xors, config)(onItemset)

  def minePreprocessed(dataset: Dataset[Set[Int]],
                       minsup: Int,
                       frequentItems0: Seq[CandidateItem],
                       xors: Seq[ParityConstraint] = Seq.empty,
                       config: EclatConfiguration  = EclatConfiguration())
                      (onItemset: Listener): Unit = {
    require(xors.isEmpty || config.geBeforeProjection || config.geAfterProjection)

    logProblem(dataset, minsup)
    logXors(xors, "raw")
    logConfig(config)

    val frequentItems = config.postSelectFrequentItems
      .map(selector => selector.apply(frequentItems0))
      .getOrElse(frequentItems0)
    xors.foldLeft(frequentItems) { case (validFrequentItems, xor) =>
      validFrequentItems.filter(i => xor.checkSingleton(i.item))
    }.foreach { i =>
      onItemset(Itemset(Set(i.item), dataset, Some(new BitsetMask(i.tids, dataset.size))))
    }

    val sortedFrequentItems = frequentItems.sortBy(_.support)
    logFrequentItems(sortedFrequentItems)

    val itemNames = sortedFrequentItems.map(_.item).toIndexedSeq
    val recodedFrequentItems = sortedFrequentItems.indices.map { i =>
      new CandidateItem(i, sortedFrequentItems(i).tids, sortedFrequentItems(i).support)
    }

    val itemCount = frequentItems.size
    val solutionHolder = PrefixWrapper(Array.fill(itemCount)(0), 0)
    val helper = new EclatHelper(config, dataset, minsup, itemNames, onItemset)

    val normalisedXors = normaliseXors(xors, itemNames)
    logXors(normalisedXors, "normalised")

    val rootGauss = ParitySystem(normalisedXors, itemCount)
    traceMatrix(rootGauss, "Original normalised matrix")
    rootGauss.echelonise()
    debugMatrix(rootGauss, "Echelonised normalised matrix")

    /* Starting the actual search */
    for (i <- recodedFrequentItems.indices.dropRight(1)) {
      val prefix = recodedFrequentItems(i)
      val itemVars = ItemVariables(itemCount)
      itemVars.pruneRange(0, i)
      itemVars.set(i)

      val gauss = rootGauss.copy()
      gauss.update(itemVars.delta)
      itemVars.delta.reset()

      propagateXorConstraints(
        solutionHolder.set(prefix.item),
        prefix.tids,
        recodedFrequentItems.drop(i + 1),
        itemVars,
        gauss,
        helper
      )
      gauss.free()
    }
    rootGauss.free()
  }

  private def processEquivalenceClass(solution0: PrefixWrapper,
                                      suffixes: Seq[CandidateItem],
                                      itemVars0: ItemVariables,
                                      gauss0: ParitySystem,
                                      helper: EclatHelper): Unit = {
    def passesXors(item: Int, gauss: ParitySystem = gauss0) = gauss.isValidExtension(item)
    def passXors(i1: Int, i2: Int) = gauss0.isValidExtension(i1, i2)

    suffixes match {
      case Seq(i) =>
        if (passesXors(i.item)) helper.output(solution0, i)
      case Seq(i1, i2) =>
        if (passesXors(i1.item)) helper.output(solution0, i1)
        if (passesXors(i2.item)) helper.output(solution0, i2)
        if (passXors(i1.item, i2.item)) helper.maybeOutput(solution0, i1, i2)
      case _ =>
        for (i <- suffixes.indices) {
          val itemVars = itemVars0.copy()
          val gauss = gauss0.copy()

          val next = suffixes(i)
          if (passesXors(next.item, gauss))
            helper.output(solution0, next)

          itemVars.set(next.item)
          suffixes.view(0, i).foreach { suf => itemVars.prune(suf.item) } // Prune skipped items
          gauss.update(itemVars.delta)
          itemVars.delta.reset()

          val candidateSuffixes = suffixes.drop(i + 1)
          if (candidateSuffixes.nonEmpty) {
            propagateXorConstraints(
              solution0.append(next.item),
              next.tids,
              candidateSuffixes,
              itemVars,
              gauss,
              helper
            )
          }
          gauss.free()
        }
    }
  }

  private def propagateXorConstraints(solution0: PrefixWrapper,
                                      solutionTids: util.BitSet,
                                      candidateSuffixes: Seq[CandidateItem],
                                      itemVars: ItemVariables,
                                      gauss: ParitySystem,
                                      helper: EclatHelper): Unit = {
    require(itemVars.delta.isEmpty)

    var solution = solution0
    logPreparingEquivalenceClass(solution, candidateSuffixes, itemVars, gauss)

    /* Propagation is implemented as a nested function */
    def propagate(candidates: Seq[CandidateItem], emit: Seq[CandidateItem] => Unit): Unit = {
      val consistent = gauss.updateThenPropagate(itemVars)
      if (!consistent) {
        logInconistency(solution)
        emit(Seq.empty)
        return
      }

      if (consistent && !itemVars.delta.isEmpty)
        breakable {
          def maybeFailDueToSupport(shouldFail: Boolean): Unit =
            if (shouldFail && solutionTids.cardinality() < helper.minsup) {
              logFailureToFastForward(solution, solutionTids.cardinality())
              emit(Seq.empty)
              break
            }

          traceVarsMatrix(gauss, itemVars, "Propagated by GE")

          var fastForwarded = false
          candidates.view.filter(cand => itemVars.delta.hasBeenSet(cand.item)).foreach { cand =>
            solutionTids.and(cand.tids)

            maybeFailDueToSupport(helper.config.checkUpdatedPrefixEarly)
            solution = solution.append(cand.item)
            fastForwarded = true
          }
          maybeFailDueToSupport(!helper.config.checkUpdatedPrefixEarly)

          val prefixIsValidSolution = fastForwarded && gauss.isValidAssignment()
          if (prefixIsValidSolution) {
            logFastForwardOutput(solution)
            helper.output(solution, solutionTids)
          }

          val updatedCandidates = candidates
            .filterNot(cand => itemVars.delta.hasBeenFixed(cand.item))
            .map(cand => cand.combine(solutionTids))

          updatedCandidates.view.filter(_.support < helper.minsup).foreach { cand =>
            itemVars.prune(cand.item)
            gauss.unsetItem(cand.item)
          }

          emit(updatedCandidates.view.filter(_.support >= helper.minsup).toIndexedSeq)
        }
    }

    var candidates = candidateSuffixes
    if (helper.config.geBeforeProjection && candidateSuffixes.size > 2) {
      logGaussianElimination(solution, "before projection")
      propagate(candidates, { candidates = _ })
      logGaussianEliminationResult(solution, "before projection", candidates, itemVars.delta)
      itemVars.delta.reset()
    }

    var frequentSuffixes: Seq[CandidateItem] = candidates.foldLeft(Seq[CandidateItem]()) { case (fs, suffixItem) =>
      val suffixCandidate = suffixItem.combine(solutionTids)
      if (suffixCandidate.support >= helper.minsup) {
        fs :+ suffixCandidate
      } else {
        itemVars.prune(suffixCandidate.item)
        fs
      }
    }
    logFrequentSuffixes(solution, frequentSuffixes, itemVars, gauss)

    if (helper.config.geAfterProjection && frequentSuffixes.size > 2) {
      logGaussianElimination(solution, " after projection")
      propagate(frequentSuffixes, { frequentSuffixes = _ })
      logGaussianEliminationResult(solution, " after projection", frequentSuffixes, itemVars.delta)
      itemVars.delta.reset()
    }

    if (frequentSuffixes.nonEmpty)
      processEquivalenceClass(solution, frequentSuffixes, itemVars, gauss, helper)
  }

  def determineFrequentItems(dataset: Dataset[Set[Int]], minsup: Int): Seq[CandidateItem] = {
    val itemCount = dataset.attributes.size

    dataset match {
      case bdb: BinaryDataset if false =>
        bdb.itemCovers.view.zipWithIndex.filter(_._1.size >= minsup).map { case (mask, i) =>
          new CandidateItem(i, BinaryDatasetHack.bitset(mask), mask.size)
        }
      case _ =>
        val bitsets = Seq.fill(itemCount)(new util.BitSet())
        val supports = Array.fill(itemCount)(0)
        dataset.records.foreach { t => t.values.foreach { i =>
          bitsets(i).set(t.index, true)
          supports(i) += 1
        }}
        (0 until itemCount).filter { i => supports(i) >= minsup }
          .map { i => new CandidateItem(i, bitsets(i), supports(i)) }
    }
  }

  private def normaliseXors(xors: Seq[ParityConstraint], itemNames: IndexedSeq[Int]) = {
    val itemIndices = itemNames.view.zipWithIndex
      .foldLeft(Map.empty[Int, Int]) { case (acc, (item, index)) => acc.updated(item, index) }
    xors.map { xor =>
      val normalisedCoefficients = xor.coefficientSet.filter(itemIndices.contains).map(itemIndices)
      new ParityConstraint(normalisedCoefficients, xor.parity)
    }
  }
}

protected object BinaryDatasetHack {
  import be.kuleuven.pmlib.patterns.Mask

  private val bitsetField = classOf[BitsetMask].getDeclaredField("be$kuleuven$pmlib$patterns$BitsetMask$$mask")
  bitsetField.setAccessible(true)

  def bitset(mask: Mask): util.BitSet =
    BitsetUtils.clone(bitsetField.get(mask).asInstanceOf[util.BitSet])
}



