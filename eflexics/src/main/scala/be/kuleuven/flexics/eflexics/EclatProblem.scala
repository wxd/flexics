package be.kuleuven.flexics.eflexics

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.weightgen
import be.kuleuven.flexics.{MaxSupport, MinSupport}

final case class EclatProblem(dataset: Dataset[Set[Int]], threshold: Int) extends weightgen.Problem[Itemset] {
  private val frequentItems0 = Eclat.determineFrequentItems(dataset, threshold)
  val frequentItems = frequentItems0.map(_.item)

  val constraints = Seq(
    new MinSupport {
      override val minsup: Int = threshold
    },
    new MaxSupport {
      override val maxsup: Int = frequentItems0.view.map(_.support).max
    }
  )

  def candidateItems = // Deep copy frequent items, because `_.tids` is mutated during mining
    frequentItems0.map(ci => new CandidateItem(ci.item, BitsetUtils.clone(ci.tids), ci.support))

  override val varIndices: IndexedSeq[Int] = frequentItems.toIndexedSeq
  override val varCount: Int = frequentItems0.size
}
