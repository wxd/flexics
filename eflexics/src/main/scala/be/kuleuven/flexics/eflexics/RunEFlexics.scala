package be.kuleuven.flexics.eflexics

import be.kuleuven.pmlib.itemsets._
import be.kuleuven.weightgen.{BoundedWeightFunction, WeightGenConfiguration, WeightMcConfiguration}
import be.kuleuven.flexics
import be.kuleuven.flexics.Support

import scala.util.Random

object RunEFlexics extends App {
  private implicit val rnd = new Random(1)

  private val dataDir = "/Users/vladimir/Documents/phd/code/_bench/cp_sampling/resources/datasets/"
  private val dataset = CP4IMData.load(s"$dataDir/vote.txt", CP4IMParameters(dropLabel = true))
  println(dataset.name)
  println(f"${dataset.attributes.size}%d items, ${dataset.size}%d transactions")

  private val minsup = 30
  private val problem = EclatProblem(dataset, minsup)
  println(s"minsup=$minsup, ${problem.frequentItems.size} frequent items")

  private val weight = Support(dataset, problem.constraints).asInstanceOf[BoundedWeightFunction[Itemset]]
  println(f"Weight: $weight%s (tilt=${weight.tiltBound}%.2f)")

  /* var n = 0
  Eclat.minePreprocessed(dataset, minsup, problem.candidateItems) { _ => n += 1 }
  println(n) */

  private val counter = EclatWeightMC
  private val sampler = EclatWeightGen
  private val wrapper = EFlexics
  private val wgConf = WeightGenConfiguration(kappa = 0.9)
  private val nSamples = 10
  for (_ <- 1 to 1) {
    var startTime = System.nanoTime()
    //val wmc = counter.count(problem, weight, WeightMcConfiguration.default)(rnd)
    //val si = wgConf.startIteration(wmc)
    var elapsed = (System.nanoTime() - startTime) / 1000000000.0
    //println(f"Full counting: $si%d (${wmc.estimate}%.1f) in $elapsed%.3fs")

    startTime = System.nanoTime()
    wrapper.countThenSample(problem, weight, wgConf, solutionWeight = Some(flexics.GetWeight), lazyCounting = false)(rnd)
      .take(nSamples).foreach(println)
    elapsed = (System.nanoTime() - startTime) / 1000000000.0
    println(f"   Estimation: in $elapsed%.3fs")
    // println(f"   Estimation: $di%d (${wmce.estimate}%.1f) in $elapsed%.3fs")
  }
}
