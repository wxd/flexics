package be.kuleuven.flexics.eflexics.test

import java.util

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.flexics.eflexics.Eclat.Listener
import be.kuleuven.flexics.eflexics.{BitsetUtils, CandidateItem}

import scala.collection.JavaConversions._


object EclatSPMF {
  private val impl = new AlgoEclat_Bitset()

  private def spmfDatabase(dataset: Dataset[Set[Int]]): TransactionDatabase =
    new TransactionDatabase {
      override def getTransaction(t: Int): java.lang.Iterable[java.lang.Integer] =
        dataset.records(t).values.map(i => java.lang.Integer.valueOf(i))

      override def size(): Int = dataset.size
    }


  def mine(dataset: Dataset[Set[Int]], minsup: Int, onItemset: Listener): Unit =
      impl.runAlgorithm(spmfDatabase(dataset), minsup, EclatListenerAdapter(onItemset, dataset))

  def minePreprocessed(dataset: Dataset[Set[Int]],
                       minsup: Int,
                       frequentItems: Seq[CandidateItem],
                       onItemset: Listener): Unit = {
    val frequentItems0 = new util.ArrayList[java.lang.Integer](frequentItems.size)
    val mapItemTIDS = new util.HashMap[java.lang.Integer, BitSetSupport](frequentItems.size)
    frequentItems.foreach { ci =>
      val bsSupport = new BitSetSupport()
      bsSupport.bitset = BitsetUtils.clone(ci.tids)
      bsSupport.support = ci.support

      mapItemTIDS.put(ci.item, bsSupport)
      frequentItems0.add(ci.item)
    }

    impl.runAlgorithm(frequentItems0, mapItemTIDS, minsup, EclatListenerAdapter(onItemset, dataset))
  }
}
