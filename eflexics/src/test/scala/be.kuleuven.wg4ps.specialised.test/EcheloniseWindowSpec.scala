package be.kuleuven.flexics.eflexics.test

import be.kuleuven.m4ri.DenseMatrixGF2
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EcheloniseWindowSpec extends UnitSpec {
  private val rnd = new Random(System.currentTimeMillis().hashCode())
  private val iterations = 1000
  private val (minRows, rowCount) = (3, 30)
  private val (minCols, colCount) = (5, 300)

  for (i <- 1 to iterations) {
    val rows = minRows + rnd.nextInt(rowCount)
    val columns = minCols + rnd.nextInt(colCount)

    val bottommostRow  = rnd.nextInt(rows - 1)
    val leftmostColumn = rnd.nextInt(columns - 2)

    s"Echelonising (0 -> $bottommostRow;$leftmostColumn -> $columns) window in ${rows}x$columns matrix (iter.$i)" should "be correct" in {
      val matrix = DenseMatrixGF2.random(rows, columns)
      matrix.clearLeftColumns(leftmostColumn)

      val windowAsMatrix = matrix.copyCorner(bottommostRow, leftmostColumn)

      matrix.echelonizeUpperRightCorner(bottommostRow, leftmostColumn, true)
      windowAsMatrix.echelonize(true)

      for (r <- 0 until windowAsMatrix.getRows; c <- 0 until windowAsMatrix.getColumns)
        assertResult(windowAsMatrix.get(r, c), s"Echelonisation error at ($r;$c)")(matrix.get(r, leftmostColumn + c))
    }
  }
}
