package be.kuleuven.flexics.eflexics.test

import java.io.File

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.{CP4IMData, CP4IMParameters, Itemset}
import be.kuleuven.weightgen.ParityConstraint
import be.kuleuven.flexics.eflexics.Eclat.EclatConfiguration
import be.kuleuven.flexics.eflexics.{Eclat, EclatProblem}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class WeightGenControllerSpec extends UnitSpec {
  val datasets = Seq("zoo-1", "vote").map { name =>
    val resourceFile = new File(getClass.getResource(s"/datasets/$name.txt").getPath)
    CP4IMData.load(name, resourceFile, CP4IMParameters(dropLabel = true))
  }
  val thresholds = Seq(0.5, 0.25, 0.15)
  val constraintCount = 15

  for (ds <- datasets;
       minsup <- thresholds.map(mf => (mf * ds.size).toInt);
       problem = EclatProblem(ds, minsup);
       baseSolutions = spmfSolve(ds, minsup);
       seed <- Seq(1, System.nanoTime().hashCode());
       rnd = new Random(seed);
       xors = Seq.fill(constraintCount)(ParityConstraint.generateFor(problem)(rnd));
       activeConstraintCount <- 0.to(constraintCount, step = 5);
       activeConstraints = xors.take(activeConstraintCount);
       before <- Seq(false, true);
       after  <- if (before) Seq(false, true) else Seq(true);
       checkOften <- Seq(false, true)) {
    val config = EclatConfiguration(geBeforeProjection = before, geAfterProjection = after, checkUpdatedPrefixEarly = checkOften)
    val configString = Seq("BEFORE", "AFTER", "OFTEN").zip(Seq(before, after, checkOften)).filter(_._2).map(_._1).mkString("{", ", ", "}")
    s"Output for ${ds.name} with minsup=$minsup, $activeConstraintCount XORs, config $configString and seed $seed" should "be identical to post-processed SPMF" in {
      val solutions = baseSolutions.filter { case (set, _) => activeConstraints.forall(_.check(set)) }
      val solutionCount = solutions.size

      var n = 0
      Eclat.mine(ds, minsup, xors = activeConstraints, config) { itemset: Itemset =>
        val items = itemset.items
        val isValidSolution = solutions contains items
        assert(isValidSolution, items)
        assertResult(solutions(items), s"$itemset has wrong support")(itemset.size)
        n += 1
      }

      assertResult(solutionCount, s"[Constraints: ${activeConstraints.mkString(", ")}]")(n)
    }
  }

  private def spmfSolve(dataset: Dataset[Set[Int]], minsup: Int) = {
    var solutions = Map[Set[Int], Int]()
    EclatSPMF.mine(dataset, minsup, { itemset => solutions = solutions.updated(itemset.items, itemset.size) })
    solutions
  }
}
