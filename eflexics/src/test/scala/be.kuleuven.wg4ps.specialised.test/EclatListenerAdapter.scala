package be.kuleuven.flexics.eflexics.test

import java.util

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.pmlib.patterns.BitsetMask

trait EclatListenerAdapter extends EclatResultListener {
  protected val dataset: Dataset[Set[Int]]

  protected def onItemset(itemset: Itemset): Unit

  override final def save(prefix: Array[Int], prefixLength: Int, suffixItem: Int, tidset: BitSetSupport): Unit =
    onItemset(Itemset(
      (0 until prefixLength).map(prefix).toSet + suffixItem,
      dataset,
      Some(new BitsetMask(tidset.bitset, dataset.size))
    ))

  override final def saveSingleItem(item: Int, support: Int, tidset: util.BitSet): Unit =
    onItemset(Itemset(Set(item), dataset, Some(new BitsetMask(tidset, dataset.size))))
}

object EclatListenerAdapter {
  def apply(callback: Itemset => Unit, ds: Dataset[Set[Int]]): EclatListenerAdapter = new EclatListenerAdapter {
      override protected def onItemset(itemset: Itemset): Unit = callback(itemset)

      override protected val dataset: Dataset[Set[Int]] = ds
    }
}