package be.kuleuven.flexics.eflexics.test;

import java.util.BitSet;

public interface EclatResultListener {
    void save(int[] prefix,
              int prefixLength,
              int suffixItem,
              BitSetSupport tidset);

    void saveSingleItem(int item,
                        int support,
                        BitSet tidset);
}
