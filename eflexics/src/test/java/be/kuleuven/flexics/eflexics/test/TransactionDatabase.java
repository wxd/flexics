package be.kuleuven.flexics.eflexics.test;

public interface TransactionDatabase {
    int size();

    Iterable<Integer> getTransaction(int i);
}
