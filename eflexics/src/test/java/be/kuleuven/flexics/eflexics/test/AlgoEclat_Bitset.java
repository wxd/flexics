package be.kuleuven.flexics.eflexics.test;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author Philippe Fournier-Viger
 */
public class AlgoEclat_Bitset {
    public final void runAlgorithm(final TransactionDatabase database,
                                   final int minsupp,
                                   final EclatResultListener... listeners) {
        final Map<Integer, BitSetSupport> mapItemTIDS = new HashMap<Integer, BitSetSupport>();
        calculateSupportSingleItems(database, mapItemTIDS);

        final List<Integer> frequentItems = new ArrayList<Integer>();
        for (Entry<Integer, BitSetSupport> entry : mapItemTIDS.entrySet()) {
            final BitSetSupport tidset = entry.getValue();
            final int support = tidset.support;
            final int item = entry.getKey();
            if (support >= minsupp) {
                frequentItems.add(item);
                saveSingleItem(item, support, tidset.bitset, listeners);
            }
        }

        // Sort items by increasing support
        Collections.sort(frequentItems, new Comparator<Integer>() {
            @Override
            public int compare(final Integer item1, final Integer item2) {
                return mapItemTIDS.get(item1).support - mapItemTIDS.get(item2).support;
            }
        });

        runAlgorithm(frequentItems, mapItemTIDS, minsupp, listeners);
    }


    public final void runAlgorithm(final List<Integer> frequentItems,
                                   final Map<Integer, BitSetSupport> mapItemTIDS,
                                   final int minsupp,
                                   final EclatResultListener... listeners) {
        final int[] itemsetBuffer = new int[frequentItems.size()];

        // Now we will combine each pairs of single items to generate equivalence classes
        // of 2-itemsets
        for (int i = 0; i < frequentItems.size(); i++) {
            final Integer itemI = frequentItems.get(i);
            final BitSetSupport tidsetI = mapItemTIDS.get(itemI);

            final List<Integer> equivalenceClassIitems = new ArrayList<Integer>();
            final List<BitSetSupport> equivalenceClassItidsets = new ArrayList<BitSetSupport>();
            for (int j = i + 1; j < frequentItems.size(); j++) {
                final int itemJ = frequentItems.get(j);
                final BitSetSupport tidsetJ = mapItemTIDS.get(itemJ);

                final BitSetSupport bitsetSupportIJ = performAND(tidsetI, tidsetJ);
                if (bitsetSupportIJ.support >= minsupp) {
                    equivalenceClassIitems.add(itemJ);
                    equivalenceClassItidsets.add(bitsetSupportIJ);
                }
            }

            if (!equivalenceClassIitems.isEmpty()) {
                itemsetBuffer[0] = itemI;
                processEquivalenceClass(
                        itemsetBuffer,
                        1,
                        equivalenceClassIitems,
                        equivalenceClassItidsets,
                        minsupp,
                        listeners
                );
            }
        }
    }

    protected int calculateSupportSingleItems(final TransactionDatabase database,
                                              final Map<Integer, BitSetSupport> mapItemTIDS) {
        int maxItemId = 0;
        for (int i = 0; i < database.size(); i++) {
            for (Integer item : database.getTransaction(i)) {
                BitSetSupport tids = mapItemTIDS.get(item);
                if (tids == null) {
                    tids = new BitSetSupport();
                    mapItemTIDS.put(item, tids);
                    if (item > maxItemId) {
                        maxItemId = item;
                    }
                }

                tids.bitset.set(i);
                tids.support++;
            }
        }
        return maxItemId;
    }

    protected BitSetSupport performAND(final BitSetSupport tidsetI, final BitSetSupport tidsetJ) {
        final BitSetSupport bitsetSupportIJ = new BitSetSupport();
        bitsetSupportIJ.bitset = (BitSet) tidsetI.bitset.clone();
        bitsetSupportIJ.bitset.and(tidsetJ.bitset);
        bitsetSupportIJ.support = bitsetSupportIJ.bitset.cardinality();
        return bitsetSupportIJ;
    }

    private void processEquivalenceClass(final int[] prefix,
                                         final int prefixLength,
                                         final List<Integer> equivalenceClassItems,
                                         final List<BitSetSupport> equivalenceClassTidsets,
                                         final int minsupp,
                                         final EclatResultListener[] listeners) {
        if (equivalenceClassItems.size() == 1) {
            final int itemI = equivalenceClassItems.get(0);
            final BitSetSupport tidsetI = equivalenceClassTidsets.get(0);
            save(prefix, prefixLength, itemI, tidsetI, listeners);

            return;
        }

        if (equivalenceClassItems.size() == 2) {
            final int itemI = equivalenceClassItems.get(0);
            final BitSetSupport tidsetI = equivalenceClassTidsets.get(0);
            save(prefix, prefixLength, itemI, tidsetI, listeners);

            final int itemJ = equivalenceClassItems.get(1);
            final BitSetSupport tidsetJ = equivalenceClassTidsets.get(1);
            save(prefix, prefixLength, itemJ, tidsetJ, listeners);

            final BitSetSupport bitsetSupportIJ = performAND(tidsetI, tidsetJ);
            if (bitsetSupportIJ.support >= minsupp) {
                prefix[prefixLength] = itemI;
                save(prefix, prefixLength + 1, itemJ, bitsetSupportIJ, listeners);
            }

            return;
        }

        for (int i = 0; i < equivalenceClassItems.size(); i++) {
            final int itemI = equivalenceClassItems.get(i);
            final BitSetSupport tidsetI = equivalenceClassTidsets.get(i);

            save(prefix, prefixLength, itemI, tidsetI, listeners);

            List<Integer> equivalenceClassISuffixItems = new ArrayList<Integer>();
            List<BitSetSupport> equivalenceITidsets = new ArrayList<BitSetSupport>();
            for (int j = i + 1; j < equivalenceClassItems.size(); j++) {
                final int itemJ = equivalenceClassItems.get(j);
                final BitSetSupport tidsetJ = equivalenceClassTidsets.get(j);

                final BitSetSupport bitsetSupportIJ = performAND(tidsetI, tidsetJ);
                if (bitsetSupportIJ.support >= minsupp) {
                    equivalenceClassISuffixItems.add(itemJ);
                    equivalenceITidsets.add(bitsetSupportIJ);
                }
            }

            if (!equivalenceClassISuffixItems.isEmpty()) {
                prefix[prefixLength] = itemI;
                processEquivalenceClass(
                        prefix,
                        prefixLength + 1,
                        equivalenceClassISuffixItems,
                        equivalenceITidsets,
                        minsupp,
                        listeners
                );
            }
        }
    }

    private void save(final int[] prefix,
                      final int prefixLength,
                      final int suffixItem,
                      final BitSetSupport tidset,
                      final EclatResultListener[] listeners) {
        for (EclatResultListener listener : listeners)
            listener.save(prefix, prefixLength, suffixItem, tidset);
    }

    private void saveSingleItem(final int item,
                                final int support,
                                final BitSet tidset,
                                final EclatResultListener[] listeners) {
        for (EclatResultListener listener : listeners)
            listener.saveSingleItem(item, support, tidset);
    }
}
