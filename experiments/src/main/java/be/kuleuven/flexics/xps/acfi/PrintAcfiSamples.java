package be.kuleuven.flexics.xps.acfi;

import de.fraunhofer.iais.mcmccount.FpTreeRepresentation;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems.ItemSet;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems.Row;
import de.fraunhofer.iais.mcmccount.RelevantItemDataRepresentation;

import java.io.File;
import java.util.Iterator;
import java.util.List;

public final class PrintAcfiSamples {
    public static void main(final String[] args) throws Exception {
        final double epsilon = 0.5;

        final String filename = args[0];
        final int threshold = Integer.parseInt(args[1]);
        final int burnIn = Integer.parseInt(args[2]);
        final int samples = Integer.parseInt(args[3]);

        final List<Row> data = McmcNumFreqItems.readData(new File(filename));
        final RelevantItemDataRepresentation relevantData = new RelevantItemDataRepresentation(data, threshold);
        final RelevantItemDataRepresentation.OriginalItemMapping mapping = relevantData.getMappingToOriginalItems();
        data.clear();
        final FpTreeRepresentation fpRep = new FpTreeRepresentation(relevantData, threshold);

        final int relevantItemCount = fpRep.getNumItems();
        final long steps = McmcNumFreqItems.computeStepNumber(relevantItemCount, epsilon);
        System.err.printf("%d relevant items, %d steps\n", relevantItemCount, steps);

        final ItemSet is = new ItemSet();
        for (int i = 0; i < burnIn; i++) {
            McmcNumFreqItems.randomWalkUsingFpTrees(fpRep, is, relevantItemCount, steps, threshold);
        }

        for (int i = 0; i < samples; i++) {
            McmcNumFreqItems.randomWalkUsingFpTrees(fpRep, is, relevantItemCount, steps, threshold);

            final Iterator<Integer> items = is.set.iterator();
            if (items.hasNext()) {
                System.out.print(mapping.getOrigItem(items.next()));
            }
            while (items.hasNext()) {
                System.out.print('+');
                System.out.print(mapping.getOrigItem(items.next()));
            }

            System.out.println(";1");
        }
    }
}
