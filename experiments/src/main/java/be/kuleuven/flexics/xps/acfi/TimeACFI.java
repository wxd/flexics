package be.kuleuven.flexics.xps.acfi;

import de.fraunhofer.iais.mcmccount.FpTreeRepresentation;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems.ItemSet;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems.Row;
import de.fraunhofer.iais.mcmccount.RelevantItemDataRepresentation;

import java.io.File;
import java.util.List;

public final class TimeACFI {
    public static void main(final String[] args) throws Exception {
        final double epsilon = 0.5;

        final String filename = args[0];
        final int threshold = Integer.parseInt(args[1]);
        final int burnIn = Integer.parseInt(args[2]);
        final int samples = Integer.parseInt(args[3]);
        final int measurements = Integer.parseInt(args[4]);

        long start = System.nanoTime();

        final List<Row> data = McmcNumFreqItems.readData(new File(filename));
        final RelevantItemDataRepresentation relevantData = new RelevantItemDataRepresentation(data, threshold);
        data.clear();
        final FpTreeRepresentation fpRep = new FpTreeRepresentation(relevantData, threshold);

        final int relevantItemCount = fpRep.getNumItems();
        final long steps = McmcNumFreqItems.computeStepNumber(relevantItemCount, epsilon);

        final double msPreprocessing = (System.nanoTime() - start) * 0.000001;

        System.err.printf("%d relevant items, %d steps\n", relevantItemCount, steps);

        for (int m = 1; m <= measurements; m++) {
            final ItemSet is = new ItemSet();

            double total = msPreprocessing;

            start = System.nanoTime();
            for (int i = 1; i <= burnIn; i++) {
                McmcNumFreqItems.randomWalkUsingFpTrees(fpRep, is, relevantItemCount, steps, threshold);

                final double msElapsed = (System.nanoTime() - start) * 0.000001;
                total += msElapsed;

                start = System.nanoTime();
            }

            System.out.printf("%d;0;%.3f\n", m, total);

            start = System.nanoTime();
            for (int i = 1; i <= samples; i++) {
                McmcNumFreqItems.randomWalkUsingFpTrees(fpRep, is, relevantItemCount, steps, threshold);
                final double msElapsed = (System.nanoTime() - start) * 0.000001;

                total += msElapsed;
                System.out.printf("%d;%d;%.3f\n", m, i, msElapsed);

                start = System.nanoTime();
            }

            System.out.printf("%d;-1;%.3f\n", m, total / samples);
        }
    }
}
