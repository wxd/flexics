package de.fraunhofer.iais.mcmccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import de.fraunhofer.iais.mcmccount.McmcNumFreqItems.Row;

/**
 * (c) 2008, Mario Boley and Henrik Grosskreutz, Fraunhofer IAIS
 * 
 * Available at:
 * http://www-kd.iai.uni-bonn.de/index.php?page=people_details&id=16 or related
 * main page.
 * 
 * Contact: Mario Boley (mario.boley@iais.fraunhofer.de) or Henrik Grosskreutz
 * (henrik.grosskreutz@iais.fraunhofer.de), Fraunhofer IAIS, Schloss
 * Birlinghoven 53754 Sankt Augustin Germany
 * 
 * 
 * LICENSING TERMS
 * 
 * This source code is provided accompanying the research paper
 * 
 * "A Randomized Approach for Approximating the Number of Frequent Sets" by M.
 * Boley and H. Grosskreutz
 * 
 * Note that the results presented in the paper may refer to an older version of
 * this program.
 * 
 * This source code is granted free of charge for research and education
 * purposes. Scientific results produced using the software provided must
 * appropriately cite the above paper. Moreover shall the authors be informed
 * about the publication.
 * 
 * The software must not be modified and distributed without prior permission of
 * the author.
 * 
 * By using this program you agree to the licensing terms.
 * 
 * 
 * NO WARRANTY
 * 
 * BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE
 * PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE
 * STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE
 * PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND
 * PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 * 
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL
 * ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE
 * THE PROGRAM, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO
 * USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
 * RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
 * OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR
 * OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

public class RelevantItemDataRepresentation {

	public class RelevantItemRows {
		Set<Integer> itemIndexes = new HashSet<Integer>();

		public void add(Integer item) {
			itemIndexes.add(item);
		}

		public boolean contains(Integer condition) {
			return itemIndexes.contains(condition);
		}

		public List<Integer> getItemsOrderedByFreq() {
			List<Integer> result = new ArrayList<Integer>(itemIndexes);
			Collections.sort(result);

			return result;
		}

		@Override
		public String toString() {
			StringBuffer result = new StringBuffer();
			result.append("[row ");
			for (Integer i : getItemsOrderedByFreq()) {
				result.append(i + " ('" + relevantItems.get(i) + "') ");
			}
			result.append("]");
			return result.toString();
		}
	}

	private final List<String> relevantItems = new ArrayList<String>();

	private final List<RelevantItemRows> rows;

	public void clearRows() {
		rows.clear();
	}

	public RelevantItemDataRepresentation(List<Row> data, int threshhold) {
		final Map<String, Integer> item2Count = new HashMap<String, Integer>();

		for (Row row : data) {
			for (String i : row.items) {
				Integer count = item2Count.get(i);
				if (count == null) {
					item2Count.put(i, 1);
				} else {
					item2Count.put(i, count + 1);
				}
			}
		}

		for (String i : item2Count.keySet()) {
			if (item2Count.get(i) >= threshhold) {
				relevantItems.add(i);
			}
		}

		// Sort relevant items wrt. frequency
		Collections.sort(relevantItems, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return -(item2Count.get(o1) - item2Count.get(o2));
			}
		});

		rows = new ArrayList<RelevantItemRows>(data.size());
		for (Row origRow : data) {
			RelevantItemRows relevantRow = new RelevantItemRows();

			for (int i = 0; i < relevantItems.size(); i++) {
				if (origRow.items.contains(relevantItems.get(i)))
					relevantRow.add(i);
			}

			rows.add(relevantRow);
		}
	}

	/**
	 * @return number of relevant items
	 */
	public int size() {
		return relevantItems.size();
	}

	public List<RelevantItemRows> getRelevantItemRows() {
		return rows;
	}

	public static class OriginalItemMapping {

		private final List<String> relevantItems;

		public OriginalItemMapping(List<String> relevantItems_) {
			relevantItems = relevantItems_;
		}

		public String getOrigItem(int i) {
			return relevantItems.get(i);
		}
	}

	public OriginalItemMapping getMappingToOriginalItems() {
		return new OriginalItemMapping(relevantItems);
	}

}
