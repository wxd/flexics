package de.fraunhofer.iais.mcmccount.util;

public class TimeProfiler {
	private long start = System.currentTimeMillis();

	public void done() {
		long stop = System.currentTimeMillis();
		System.out.println("Time: " + (stop - start) + " ms");
	}
}
