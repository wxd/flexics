package de.fraunhofer.iais.mcmccount.util;

/**
 * @author Henrik Grosskreutz
 * 
 * A pair of two object of class C1 resp. C2
 */
public class Pair<C1, C2> {

	private final C1 first;
	private final C2 second;

	public Pair(C1 first, C2 second) {
		this.first = first;
		this.second = second;
	}

	public C1 first() {
		return first;
	}

	public C2 second() {
		return second;
	}

	public String toString() {
		return "<" + first.toString() + "," + second.toString() + ">";
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Pair) {
			Pair otherPair = (Pair) other;
			if (first == null) {
				if (otherPair.first != null)
					return false;
			} else if (!first.equals(otherPair.first))
				return false;

			if (second == null) {
				if (otherPair.second != null)
					return false;
			} else if (!second.equals(otherPair.second))
				return false;

			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (first == null && second == null)
			return 0;

		int result = 0;
		if (first != null)
			result += first.hashCode();
		if (second != null)
			result += second.hashCode();

		return result;
	}
}
