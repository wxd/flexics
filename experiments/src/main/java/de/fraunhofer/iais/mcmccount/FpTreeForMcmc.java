package de.fraunhofer.iais.mcmccount;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fraunhofer.iais.mcmccount.FPTreeNode.Matcher;
import de.fraunhofer.iais.mcmccount.FPTreeNode.ParentItemCache;
import de.fraunhofer.iais.mcmccount.RelevantItemDataRepresentation.OriginalItemMapping;
import de.fraunhofer.iais.mcmccount.RelevantItemDataRepresentation.RelevantItemRows;
import de.fraunhofer.iais.mcmccount.util.McmcUtil;

/**
 * (c) 2008, Mario Boley and Henrik Grosskreutz, Fraunhofer IAIS
 * 
 * Available at:
 * http://www-kd.iai.uni-bonn.de/index.php?page=people_details&id=16 or related
 * main page.
 * 
 * Contact: Mario Boley (mario.boley@iais.fraunhofer.de) or Henrik Grosskreutz
 * (henrik.grosskreutz@iais.fraunhofer.de), Fraunhofer IAIS, Schloss
 * Birlinghoven 53754 Sankt Augustin Germany
 * 
 * 
 * LICENSING TERMS
 * 
 * This source code is provided accompanying the research paper
 * 
 * "A Randomized Approach for Approximating the Number of Frequent Sets" by M.
 * Boley and H. Grosskreutz
 * 
 * Note that the results presented in the paper may refer to an older version of
 * this program.
 * 
 * This source code is granted free of charge for research and education
 * purposes. Scientific results produced using the software provided must
 * appropriately cite the above paper. Moreover shall the authors be informed
 * about the publication.
 * 
 * The software must not be modified and distributed without prior permission of
 * the author.
 * 
 * By using this program you agree to the licensing terms.
 * 
 * 
 * NO WARRANTY
 * 
 * BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE
 * PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE
 * STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE
 * PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND
 * PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 * 
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL
 * ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE
 * THE PROGRAM, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO
 * USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
 * RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
 * OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR
 * OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

public class FpTreeForMcmc {
	private FPTreeNode root;

	private FPTreeHeader[] headers;
	private int totalCount = 0;
	public final List<Integer> conditionedOn = new ArrayList<Integer>();
	private int maxItem;
	private OriginalItemMapping itemMapping;

	public FpTreeForMcmc(RelevantItemDataRepresentation relevantData) {
		maxItem = relevantData.size() - 1;

		root = new FPTreeNode(-1, null, maxItem);

		headers = new FPTreeHeader[maxItem + 1];
		for (int i = 0; i < headers.length; i++) {
			headers[i] = new FPTreeHeader(i);
		}

		for (RelevantItemRows r : relevantData.getRelevantItemRows()) {
			addRecord(r);
		}

		itemMapping = relevantData.getMappingToOriginalItems();
	}

	private FpTreeForMcmc(FpTreeForMcmc parent, Integer lastConditional) {

		conditionedOn.addAll(parent.conditionedOn);
		conditionedOn.add(lastConditional);
		maxItem = lastConditional - 1;

		root = new FPTreeNode(-1, null, maxItem);

		headers = new FPTreeHeader[maxItem + 1];
		for (int i = 0; i < headers.length; i++) {
			headers[i] = new FPTreeHeader(i);
		}

		itemMapping = parent.itemMapping;
	}

	public int getTotalCount() {
		return totalCount;
	}

	// This implementation is based on the function "insert_tree" of Han, Pei &
	// Yin 2000
	public void addRecord(RelevantItemRows row) {
		addRecord(row.getItemsOrderedByFreq(), 1);
	}

	private void addRecord(List<Integer> itemsOrdered, int counter) {
		Map<Integer, FPTreeNode> level = root.children;
		FPTreeNode node = root;
		for (Integer termPrim : itemsOrdered) {
			Integer term = McmcUtil.getRepresentative(termPrim);

			if (level.containsKey(term)) {
				node = level.get(term);
				level = node.children;
			} else {
				node = new FPTreeNode(term, node, maxItem);
				headers[term].addNode(node);
				level.put(term, node);
				level = node.children;
			}
			node.increaseCounters(counter);
		}

		totalCount += counter;
	}

	public boolean isNumOccurencesAbove(int[] itemsOrdered, int frequencyThreshold) {
		int startIndex = 0;

		while (conditionedOn.size() > startIndex && itemsOrdered[startIndex] == conditionedOn.get(startIndex)) {
			startIndex++;
		}

		if (itemsOrdered.length <= startIndex) {
			return totalCount >= frequencyThreshold;
		}

		FPTreeHeader header = headers[itemsOrdered[startIndex]];

		Matcher matcher = ParentItemCache.getMatcher(itemsOrdered, startIndex, maxItem);

		int result = 0;
		for (FPTreeNode node : header.getNodesList()) {
			if (matcher.matches(node)) {
				result += node.counter;
			}

			if (result >= frequencyThreshold)
				return true;
		}

		return false;
	}

	public int getCount(int[] itemsOrdered) {
		int startIndex = 0;

		while (conditionedOn.size() > startIndex && itemsOrdered[startIndex] == conditionedOn.get(startIndex)) {
			startIndex++;
		}

		if (itemsOrdered.length <= startIndex) {
			return totalCount;
		}

		FPTreeHeader header = headers[itemsOrdered[startIndex]];

		Matcher matcher = ParentItemCache.getMatcher(itemsOrdered, startIndex, maxItem);

		int result = 0;
		for (FPTreeNode node : header.getNodesList()) {
			if (matcher.matches(node)) {
				result += node.counter;
			}
		}

		return result;
	}

	public int getCount(int item) {
		FPTreeHeader header = headers[item];

		int result = 0;
		for (FPTreeNode node : header.getNodesList()) {
			result += node.counter;
		}

		return result;
	}

	public FpTreeForMcmc getConditionalFPTree(Integer item, int threshold) {
		for (int i = 0; i < item; i++) {
			if (getCount(i) < threshold) {
				System.err.println("Item " + threshold + " is to infreq!");
			}
		}

		FpTreeForMcmc result = new FpTreeForMcmc(this, item);

		FPTreeHeader header = headers[item];

		for (FPTreeNode node : header.getNodesList()) {
			result.addRecord(node.parentItemInfo.getItemsOrdered(maxItem), node.counter);
		}

		return result;
	}

	public String toString() {
		StringBuffer sbTree = new StringBuffer();
		sbTree.append("FP-Tree \n - Conditioned on (");

		boolean isFirst;
		sbTree.append(getConditionedOnString());

		sbTree.append(")");

		sbTree.append("\n - Headers (");
		isFirst = true;
		for (FPTreeHeader h : headers) {
			if (isFirst)
				isFirst = false;
			else
				sbTree.append(",");
			sbTree.append("(index: " + h.item + ", orig: " + itemMapping.getOrigItem(h.item) + ")");
		}
		sbTree.append(")\n");

		return sbTree.toString();
	}

	private String getConditionedOnString() {
		StringBuffer result = new StringBuffer();
		boolean isFirst = true;
		for (Integer i : conditionedOn) {
			if (isFirst)
				isFirst = false;
			else
				result.append(",");
			result.append("(index: " + i + ", orig: " + itemMapping.getOrigItem(i) + ")");
		}

		return result.toString();
	}

	public static class FPTreeHeader {

		private List<FPTreeNode> nodes = new ArrayList<FPTreeNode>();
		private int item;

		public FPTreeHeader(int i) {
			item = i;
		}

		public void addNode(FPTreeNode node) {
			nodes.add(node);
		}

		public List<FPTreeNode> getNodesList() {
			return nodes;
		}

		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append("(");
			sb.append(")--> ");
			sb.append(nodes.toString());
			return sb.toString();
		}

		@Override
		public int hashCode() {
			return item;
		}

		@Override
		public boolean equals(Object obj) {
			return this == obj;
		}
	}

	public long calcNumWithFpGrowth(int threshold) {
		long result = 0;

		if (getTotalCount() >= threshold) {
			result++;
		}

		for (int i = 0; i <= maxItem; i++) {
			FpTreeForMcmc cond = getConditionalFPTree(i, threshold);

			result += cond.calcNumWithFpGrowth(threshold);
		}

		return result;
	}

	public void writeTo(PrintStream out) {
		writeTo(out, null);
	}

	public void writeTo(PrintStream out, Integer maxItemIndexOfNull) {
		String prefix = "";

		if (headers.length == 0) {
			// this tree has no inner nodes

			for (int i = 0; i < totalCount; i++) {
				out.println(prefix);
			}

			return;
		}

		HashMap<FPTreeNode, Integer> visitedHeaderCounts = new HashMap<FPTreeNode, Integer>();
		for (int i = headers.length - 1; i >= 0; i--) {
			FPTreeHeader header = headers[i];
			for (FPTreeNode node : header.getNodesList()) {

				int restCounter = node.counter;
				Integer already = visitedHeaderCounts.get(node);
				if (already != null)
					restCounter -= already;

				if (restCounter == 0)
					continue;

				StringBuffer row = new StringBuffer();
				row.append(prefix);

				FPTreeNode p = node;
				while (p != null) {
					Integer vhc = visitedHeaderCounts.get(p);
					if (vhc == null)
						vhc = 0;
					visitedHeaderCounts.put(p, new Integer(vhc + restCounter));

					if (!p.isRoot() && (maxItemIndexOfNull == null || p.item <= maxItemIndexOfNull))
						row.append(p.item + " ");
					p = p.parent;
				}

				for (int j = 0; j < restCounter; j++) {
					out.println(row);
				}
			}
		}
	}
}
