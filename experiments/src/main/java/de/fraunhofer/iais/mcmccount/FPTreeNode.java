/**
 * 
 */
package de.fraunhofer.iais.mcmccount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * (c) 2008, Mario Boley and Henrik Grosskreutz, Fraunhofer IAIS
 * 
 * Available at:
 * http://www-kd.iai.uni-bonn.de/index.php?page=people_details&id=16 or related
 * main page.
 * 
 * Contact: Mario Boley (mario.boley@iais.fraunhofer.de) or Henrik Grosskreutz
 * (henrik.grosskreutz@iais.fraunhofer.de), Fraunhofer IAIS, Schloss
 * Birlinghoven 53754 Sankt Augustin Germany
 * 
 * 
 * LICENSING TERMS
 * 
 * This source code is provided accompanying the research paper
 * 
 * "A Randomized Approach for Approximating the Number of Frequent Sets" by M.
 * Boley and H. Grosskreutz
 * 
 * Note that the results presented in the paper may refer to an older version of
 * this program.
 * 
 * This source code is granted free of charge for research and education
 * purposes. Scientific results produced using the software provided must
 * appropriately cite the above paper. Moreover shall the authors be informed
 * about the publication.
 * 
 * The software must not be modified and distributed without prior permission of
 * the author.
 * 
 * By using this program you agree to the licensing terms.
 * 
 * 
 * NO WARRANTY
 * 
 * BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE
 * PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE
 * STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE
 * PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND
 * PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 * 
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL
 * ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE
 * THE PROGRAM, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO
 * USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
 * RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
 * OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR
 * OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

public class FPTreeNode {
	static final int bitsPerToken = 32;

	public abstract static class Matcher {
		public abstract boolean matches(FPTreeNode node);
	}

	public static class SetMatcher extends Matcher {

		private final int[] itemsOrdered;
		private final int startIndex;
		private final int maxItem;

		public SetMatcher(int[] itemsOrdered, int startIndex, int maxItem) {
			this.itemsOrdered = itemsOrdered;
			this.startIndex = startIndex;
			this.maxItem = maxItem;
		}

		@Override
		public boolean matches(FPTreeNode node) {
			for (int i = startIndex + 1; i < itemsOrdered.length; i++) {
				if (!((ParentItemsAsSet) node.parentItemInfo).items.contains(itemsOrdered[i])) {
					return false;
				}
			}

			return true;

		}
	}

	public static class BitmapMatcher extends Matcher {
		int[] bitRepr;

		public BitmapMatcher(int[] itemsOrdered, int startIndex, int maxItem) {
			bitRepr = new int[(int) Math.ceil((double) (maxItem + 1) / bitsPerToken)];
			for (int i = startIndex + 1; i < itemsOrdered.length; i++) {
				int item = itemsOrdered[i];
				int high = item / bitsPerToken;
				int low = item - high * bitsPerToken;

				int addPattern = 1 << low;

				bitRepr[high] |= addPattern;
			}
		}

		public boolean matches(FPTreeNode node) {
			for (int i = 0; i < bitRepr.length; i++) {
				if (bitRepr[i] != 0
						&& ((bitRepr[i] & ((ParentItemIntArray) node.parentItemInfo).parentItemTokenMap[i]) != bitRepr[i]))
					return false;
			}

			return true;
		}

	}

	public static int LIMIT_FOR_ARRAY_IMP = 255;

	public static abstract class ParentItemCache {

		public static ParentItemCache create(int maxItem) {
			if (maxItem < LIMIT_FOR_ARRAY_IMP)
				return new ParentItemIntArray(maxItem);

			return new ParentItemsAsSet();
		}

		public abstract void add(FPTreeNode pointer);

		public static Matcher getMatcher(int[] itemsOrdered, int startIndex, int maxItem) {
			if (maxItem < LIMIT_FOR_ARRAY_IMP)
				return new BitmapMatcher(itemsOrdered, startIndex, maxItem);

			return new SetMatcher(itemsOrdered, startIndex, maxItem);
		}

		public abstract List<Integer> getItemsOrdered(int maxItem);

	}

	public static class ParentItemsAsSet extends ParentItemCache {

		HashSet<Integer> items = new HashSet<Integer>();

		@Override
		public void add(FPTreeNode pointer) {
			items.add(pointer.item);
		}

		@Override
		public List<Integer> getItemsOrdered(int maxItem) {
			List<Integer> itemsOrdered = new ArrayList<Integer>();

			for (int item = 0; item <= maxItem; item++) {
				if (items.contains(item))
					itemsOrdered.add(item);
			}

			return itemsOrdered;
		}
	}

	public static class ParentItemIntArray extends ParentItemCache {

		public ParentItemIntArray(int maxItem) {
			parentItemTokenMap = new int[(int) Math.ceil((double) (maxItem + 1) / bitsPerToken)];
		}

		int[] parentItemTokenMap;

		@Override
		public void add(FPTreeNode pointer) {
			int high = pointer.item / bitsPerToken;
			int low = pointer.item - high * bitsPerToken;

			int addPattern = 1 << low;

			parentItemTokenMap[high] |= addPattern;
		}

		@Override
		public String toString() {
			StringBuffer result = new StringBuffer();
			for (int i = 0; i < parentItemTokenMap.length; i++) {
				result.append(parentItemTokenMap[i] + " ");
			}

			return result.toString();
		}

		@Override
		public List<Integer> getItemsOrdered(int maxItem) {
			List<Integer> itemsOrdered = new ArrayList<Integer>();
			for (int item = 0; item <= maxItem; item++) {

				int high = item / bitsPerToken;
				int low = item - high * bitsPerToken;

				int addPattern = 1 << low;

				if ((parentItemTokenMap[high] & addPattern) != 0)
					itemsOrdered.add(item);
			}
			return itemsOrdered;
		}
	}

	int item;
	public Map<Integer, FPTreeNode> children = new HashMap<Integer, FPTreeNode>();
	public final FPTreeNode parent;
	int counter = 0;
	ParentItemCache parentItemInfo;

	public FPTreeNode(Integer item_, FPTreeNode p, int maxItem) {
		item = item_;
		parent = p;

		parentItemInfo = ParentItemCache.create(maxItem);

		if (p != null) {
			FPTreeNode pointer = p;
			while (pointer.parent != null) {
				// parentItems.add(pointer.item);

				parentItemInfo.add(pointer);
				pointer = pointer.parent;
			}
		}
	}

	public void increaseCounters(int n) {
		counter += n;
	}

	public FPTreeNode getParent() {
		return parent;
	}

	public boolean isRoot() {
		return parent == null;
	}

	public boolean isLeav() {
		return children.size() == 0;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("FpTree-Node(");
		sb.append("item: " + item + ", count: " + counter + ", parentMap: ");

		sb.append(parentItemInfo.toString());

		sb.append(")");
		return sb.toString();
	}
}