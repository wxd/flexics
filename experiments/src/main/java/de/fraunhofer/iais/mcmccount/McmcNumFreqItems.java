package de.fraunhofer.iais.mcmccount;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import de.fraunhofer.iais.mcmccount.RelevantItemDataRepresentation.RelevantItemRows;
import de.fraunhofer.iais.mcmccount.util.McmcUtil;
import de.fraunhofer.iais.mcmccount.util.Pair;
import de.fraunhofer.iais.mcmccount.util.TimeProfiler;

/**
 * (c) 2008, Mario Boley and Henrik Grosskreutz, Fraunhofer IAIS
 * 
 * Available at:
 * http://www-kd.iai.uni-bonn.de/index.php?page=people_details&id=16 or related
 * main page.
 * 
 * Contact: Mario Boley (mario.boley@iais.fraunhofer.de) or Henrik Grosskreutz
 * (henrik.grosskreutz@iais.fraunhofer.de), Fraunhofer IAIS, Schloss
 * Birlinghoven 53754 Sankt Augustin Germany
 * 
 * 
 * LICENSING TERMS
 * 
 * This source code is provided accompanying the research paper
 * 
 * "A Randomized Approach for Approximating the Number of Frequent Sets" by M.
 * Boley and H. Grosskreutz
 * 
 * Note that the results presented in the paper may refer to an older version of
 * this program.
 * 
 * This source code is granted free of charge for research and education
 * purposes. Scientific results produced using the software provided must
 * appropriately cite the above paper. Moreover shall the authors be informed
 * about the publication.
 * 
 * The software must not be modified and distributed without prior permission of
 * the author.
 * 
 * By using this program you agree to the licensing terms.
 * 
 * 
 * NO WARRANTY
 * 
 * BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE
 * PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE
 * STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE
 * PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND
 * PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 * 
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL
 * ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE
 * THE PROGRAM, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO
 * USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
 * RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
 * OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR
 * OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

public class McmcNumFreqItems {
	private static int totalwalks = 0;

	private static enum RunMode {
		FULL_MC, EXACT, HYBRID

	}

	private static RunMode mode = RunMode.HYBRID;

	private static Random rnd = new Random();

	public static class Row {
		Set<String> items = new HashSet<String>();

		public void add(String item) {
			items.add(item);
		}
	}

	private static long data_access_count = 0;

	private static String externalMiner = "d:\\EclipseWorkspace\\FpZhu\\fim_all";

        private static String processIdentifier = null;

	public static void main(String[] args) throws IOException {

		if (args.length != 4) {
			System.out.println("Usage: McmcNumFreqItems <dataset> <threshold> <externalminer> <processIdentifier>");
			System.exit(-1);
		}

		String filename = args[0];
		int threshold = Integer.parseInt(args[1]);
		externalMiner = args[2];
		processIdentifier = args[3];

		double epsilon = 0.5;

		calcNumFreqItems(filename, threshold, epsilon);
		System.out.println("number1 " + number1);
		System.out.println("number2 " + number2);
		System.out.println("number3 " + number3);
		System.out.println("number4 " + number4);
	}

	public static double calcNumFreqItems(String filename, int threshold, double epsilon) throws IOException {
		McmcUtil.showMemInfo();

		System.out.println("Reading example set " + filename + ", using threshhold " + threshold);
		List<Row> data = readData(new File(filename));

		McmcUtil.showMemInfo();
		System.out.println("Preprocessing");

		RelevantItemDataRepresentation relevantData = new RelevantItemDataRepresentation(data, threshold);
		data.clear();

		McmcUtil.showMemInfo();

		System.out.println("Found " + relevantData.size() + " relevant items");

		long ms0 = System.currentTimeMillis();

		FpTreeRepresentation fpRep = new FpTreeRepresentation(relevantData, threshold);

		McmcUtil.showMemInfo();
		System.out.println("Built tree");

		System.out.println("Starting calculation");

		long ms1 = System.currentTimeMillis();

		double result = count(fpRep, epsilon, threshold);
		long ms2 = System.currentTimeMillis();

		System.out.println("result " + result + " in " + (ms2 - ms1) + " ms (resp. " + (ms2 - ms0)
				+ " with fptree construction)");
		System.out.println("Data accesses: " + data_access_count);

		return result;
	}

	public static List<Row> readData(File file) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(file));

		List<Row> result = new ArrayList<Row>();
		String line = in.readLine();

		HashMap<String, String> flyweightMap = new HashMap<String, String>();
		while (line != null) {
			String[] items = line.split("\\s");
			Row row = new Row();
			for (int i = 0; i < items.length; i++) {
				String item = items[i];
				String itemRepr = flyweightMap.get(item);
				if (itemRepr == null) {
					itemRepr = item;
					flyweightMap.put(item, itemRepr);
				}
				row.add(itemRepr);

			}
			result.add(row);

			line = in.readLine();
		}

		return result;
	}

	public static Pair<Integer, Long> computeStartingFactor(FpTreeRepresentation fpRep, int threshold, double epsilon) {
		long fisFoundByExhaustiveSearch = 1;

		System.out.println("Computing first factors using exhaustive fpgrowth:");

		int index = 0;

		int numItems = fpRep.getNumItems();

		boolean computeExact = (mode == RunMode.EXACT);

		boolean allAtOnce = false;

		TimeProfiler p = new TimeProfiler();

		boolean usePriorMinIndex = true;
		if (usePriorMinIndex) {
			index = calcMinAprioriIndex(epsilon, numItems);

			System.out.print(index + " ");

			fisFoundByExhaustiveSearch = calcFisExact(fpRep, threshold, index - 1);
		}

		while (index < numItems
				&& (computeExact || (fisFoundByExhaustiveSearch
						+ computeCostForMcmcCalculation(epsilon, numItems, index + 1) < computeCostForMcmcCalculation(
						epsilon, numItems, index)))) {

			// Heuristic: There cannot be more than numItems new fis in the next
			// step; thus stop exhaustive search
			// if the number of samples is smaller than the number of fis to
			// discover using exhaustive search

			if (!allAtOnce) {
				FpTreeForMcmc tree = fpRep.getTree(index);
				fisFoundByExhaustiveSearch += calcFisExact(threshold, tree);
			}
			index++;

			System.out.print(index + " ");
		}
		System.out.println();

		p.done();

		System.out.println("total: " + fisFoundByExhaustiveSearch);

		System.out.println("starting mcmc at index " + index + " (up to " + numItems + ")");

		return new Pair<Integer, Long>(index, fisFoundByExhaustiveSearch);

	}

	private static int calcMinAprioriIndex(double epsilon, int numItems) {
		double lowestCost = Long.MAX_VALUE;
		int lowestCostIndex = -1;

		for (int i = 1; i < numItems; i++) {
			double cost = getExhaustiveRuntimeEstimate(i);
			cost += computeCostForMcmcCalculation(epsilon, numItems, i);

			// System.out.println("Cost for " + i + ": " + cost);
			if (cost < lowestCost) {
				lowestCost = cost;
				lowestCostIndex = i;
			}
		}

		return lowestCostIndex;
	}

	private static double computeCostForMcmcCalculation(double epsilon, int numItems, int startIndex) {
		long cost = 0;
		for (int j = startIndex + 1; j < numItems; j++) {
			cost += computeTrialNumber(numItems - (startIndex + 1), epsilon) * computeStepNumber(j, epsilon);
		}
		return cost;
	}

	private static double getExhaustiveRuntimeEstimate(int numItems) {
		return Math.pow(2, numItems);
	}

	private static long calcFisExact(FpTreeRepresentation fpRep, int threshold, int maxIndex) {

		File dsFile = getTmpFileForExternalMiner();
		try {
			System.out.println("Writing temporary file " + dsFile);
			
			fpRep.getUnconditionedTree().writeTo(new PrintStream(dsFile), maxIndex);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to call external miner!");
		}

		long proofCheck = calcFisUsingExternalMiner(threshold, dsFile);
		return proofCheck;
	}

	private static long calcFisExact(int threshold, FpTreeForMcmc tree) {
		try {
			File dsFile = getTmpFileForExternalMiner();

			System.out.println("Writing temporary file " + dsFile);
			tree.writeTo(new PrintStream(dsFile));

			return calcFisUsingExternalMiner(threshold, dsFile);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to call external miner!");
		}
	}

	public static long calcFisUsingExternalMiner(int threshold, File dataset) {
		try {

			long externalResult;
			String[] cmdarray = new String[] { externalMiner, dataset.getAbsolutePath(), "" + threshold };
			Process p = Runtime.getRuntime().exec(cmdarray);
			InputStream pResult = p.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(pResult));
			String l = r.readLine();

			int n = 0;
			try {
				n = Integer.parseInt(l);
			} catch (Exception e) {
			}
			externalResult = n + 1;
			return externalResult;
		} catch (Exception e) {
			System.err.println(e);
			throw new RuntimeException(e);
		}
	}

	private static File getTmpFileForExternalMiner() {
	    return new File( processIdentifier + "mcmcTmpDataset" +
			     rnd.nextLong() + ".txt");
	}

	static double count(FpTreeRepresentation fpRep, double epsilon, int threshhold) {
		int startingIndex = 0;
		long startingFactor = 1;

		boolean useExhaustiveAtStart = !(mode == RunMode.FULL_MC);
		if (useExhaustiveAtStart) {
			Pair<Integer, Long> pair = computeStartingFactor(fpRep, threshhold, epsilon);
			startingIndex = pair.first();
			startingFactor = pair.second();
		}

		int relevantItemCount = fpRep.getNumItems();

		long trialNumber = computeTrialNumber(relevantItemCount - startingIndex, epsilon);
		long maxSteps = computeStepNumber(relevantItemCount, epsilon);

		System.out.println("trials per factor...." + trialNumber);
		System.out.println("max steps per walk..." + maxSteps);

		double result = 1.0;
		for (int k = relevantItemCount; k > startingIndex; k--) {
			number3++;
			result = result * apxReciprocal(fpRep, k, trialNumber, computeStepNumber(k, epsilon), threshhold);
			System.out.print(".");
		}
		return (startingFactor / result);
		// ~ return (result);
	}

	public static long computeStepNumber(int itemBound, double epsilon) {
		return (int) (((double) itemBound) * (Math.max(1.0, Math.log(itemBound)) / epsilon));
	}

	private static long computeTrialNumber(int numFactorsToEstimate, double epsilon) {
		return (int) (((double) 5 * numFactorsToEstimate) / (epsilon * epsilon));
	}

	public static class ItemSet {
		public Set<Integer> set = new HashSet<Integer>();
		private int[] itemsOrderedRev;

		public boolean contains(int i) {
			return set.contains(i);
		}

		public void remove(int itemIndex) {
			set.remove(itemIndex);
			calcItemsOrdered();
		}

		private void calcItemsOrdered() {
			ArrayList<Integer> itemsOrderedList = new ArrayList<Integer>(set);

			Collections.sort(itemsOrderedList);
			int n = itemsOrderedList.size() - 1;

			itemsOrderedRev = new int[set.size()];
			for (int i = 0; i < set.size(); i++) {
				itemsOrderedRev[i] = itemsOrderedList.get(n - i);
			}
		}

		public void add(int itemIndex) {
			set.add(itemIndex);
			calcItemsOrdered();
		}

		public boolean matches(RelevantItemRows row) {
			for (Integer i : set) {
				if (!row.contains(i))
					return false;
			}
			return true;
		}

		public int[] getItemsOrderedByReverseFreq() {
			return itemsOrderedRev;
		}
	}

	static double apxReciprocal(FpTreeRepresentation fpRep, int k, long trials, long steps, int threshhold) {
		double sum = 0.0;
		for (int i = 0; i < trials; i++) {
			number4++;
			ItemSet is = new ItemSet();

			randomWalkUsingFpTrees(fpRep, is, k, steps, threshhold);

			if (!is.contains(k - 1))
				sum = sum + 1.0;
		}
		return sum / trials;
	}

	/*
	 * Returns the k-terminated rand string and the index of the last occurence
	 * of k
	 */
	private static Pair<Integer[], Integer> generateKTerminatedRandString(int itemBound, long maxlength) {
		List<Integer> resultList = new ArrayList<Integer>();

		int lastKindex = -1;
		int nextToLastKindex = -1;

		int currentIndex = 0;
		for (int i = 0; i < maxlength; i++) {
			if (rnd.nextBoolean()) {
				int nextInt = rnd.nextInt(itemBound);
				resultList.add(nextInt);
				if (nextInt == itemBound - 1) {
					nextToLastKindex = lastKindex;
					lastKindex = currentIndex;
				}

				currentIndex++;
			}
		}

		int len = lastKindex + 1;
		Integer[] result = new Integer[len];
		for (int i = 0; i < len; i++) {
			result[i] = resultList.get(i);
		}

		return new Pair<Integer[], Integer>(result, nextToLastKindex);
	}
	
	public static int number1 = 0;
	public static int number2 = 0;
	public static int number3 = 0;
	public static int number4 = 0;
	public static int i = 0;

	public static void randomWalkUsingFpTrees(FpTreeRepresentation fpRep, ItemSet is, int itemBound, long steps,
											  int frequencyThreshold) {
		totalwalks++;

		Pair<Integer[], Integer> stepsAndIndex = generateKTerminatedRandString(itemBound, steps);
		Integer[] nettoSteps = stepsAndIndex.first();
		int nextToLastKindex = stepsAndIndex.second();

		for (int k = 0; k < nettoSteps.length; k++) {
			number1++;
			int itemIdx = nettoSteps[k];

			if (is.contains(itemIdx)) {
				is.remove(itemIdx);
			} else {
				is.add(itemIdx);

				if (!fpRep.checkIfIsFrequent(is, frequencyThreshold)) {
					is.remove(itemIdx);
				} else {
					// an item has just been added
					if (itemIdx == (itemBound - 1) && k == nextToLastKindex) {
						// and it is the last item, and the next-to-last
						// occurence of that item

						// As only the occurrence of this item matters, we can
						// remove it and ignore the rest of the rnd-sequence
						is.remove(itemIdx);
						return;
					}
				}
			}
		}
	}

	public static int calculateSupportBruteForce(RelevantItemDataRepresentation relevantData, ItemSet is) {
		int result = 0;
		for (RelevantItemRows row : relevantData.getRelevantItemRows()) {
			if (is.matches(row))
				result++;
		}

		return result;
	}
}
