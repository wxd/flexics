package de.fraunhofer.iais.mcmccount;

import java.util.ArrayList;
import java.util.List;

import de.fraunhofer.iais.mcmccount.McmcNumFreqItems.ItemSet;
import de.fraunhofer.iais.mcmccount.util.McmcUtil;

/**
 * (c) 2008, Mario Boley and Henrik Grosskreutz, Fraunhofer IAIS
 * 
 * Available at:
 * http://www-kd.iai.uni-bonn.de/index.php?page=people_details&id=16 or related
 * main page.
 * 
 * Contact: Mario Boley (mario.boley@iais.fraunhofer.de) or Henrik Grosskreutz
 * (henrik.grosskreutz@iais.fraunhofer.de), Fraunhofer IAIS, Schloss
 * Birlinghoven 53754 Sankt Augustin Germany
 * 
 * 
 * LICENSING TERMS
 * 
 * This source code is provided accompanying the research paper
 * 
 * "A Randomized Approach for Approximating the Number of Frequent Sets" by M.
 * Boley and H. Grosskreutz
 * 
 * Note that the results presented in the paper may refer to an older version of
 * this program.
 * 
 * This source code is granted free of charge for research and education
 * purposes. Scientific results produced using the software provided must
 * appropriately cite the above paper. Moreover shall the authors be informed
 * about the publication.
 * 
 * The software must not be modified and distributed without prior permission of
 * the author.
 * 
 * By using this program you agree to the licensing terms.
 * 
 * 
 * NO WARRANTY
 * 
 * BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE
 * PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE
 * STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE
 * PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND
 * PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 * 
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL
 * ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE
 * THE PROGRAM, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO
 * USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
 * RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
 * OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR
 * OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

public class FpTreeRepresentation {

	private FpTreeForMcmc[] fpTrees = null;
	private FpTreeForMcmc uncondTree;
	int numItems;

	private final int threshold;

	public FpTreeRepresentation(RelevantItemDataRepresentation relevantData, int threshold) {
		this.threshold = threshold;
		System.out.print("Building FpTree representation");

		numItems = relevantData.size();

		uncondTree = new FpTreeForMcmc(relevantData);
		relevantData.clearRows();

		System.out.print("|");

		if (McmcUtil.isFreeMemLargerThanUsed(2)) {
			fpTrees = new FpTreeForMcmc[numItems];
			for (int i = 0; i < numItems; i++) {
				if (!McmcUtil.isFreeMemLargerThanUsed(2)) {
					McmcUtil.showMemInfo();

					System.out.println("Remaining conditional FpTrees (index " + i + " to " + numItems
							+ ") were not constructed for space reasons");
					break;
				}

				fpTrees[i] = uncondTree.getConditionalFPTree(i, threshold);

				System.out.print("." + i);
			}
		}

		System.out.println("");
	}

	public int getNumItems() {
		return numItems;
	}

	public int getFrequency(ItemSet is) {
		int[] itemsOrdered = is.getItemsOrderedByReverseFreq();

		if (fpTrees != null && fpTrees[itemsOrdered[0]] != null)
			return uncondTree.getCount(itemsOrdered);

		return fpTrees[itemsOrdered[0]].getCount(itemsOrdered);
	}

	public boolean checkIfIsFrequent(ItemSet is, int frequencyThreshold) {
		McmcNumFreqItems.number2++;
		int[] itemsOrdered = is.getItemsOrderedByReverseFreq();
		if (true)
			if (fpTrees != null && fpTrees[itemsOrdered[0]] != null) {
				return fpTrees[itemsOrdered[0]].isNumOccurencesAbove(itemsOrdered, frequencyThreshold);
			} else {
				return uncondTree.isNumOccurencesAbove(itemsOrdered, frequencyThreshold);
			}

		int oldNum = fpTrees[itemsOrdered[0]].getCount(itemsOrdered);

		// return oldVal;
		List<FpTreeForMcmc> trees = getConditionalTrees(itemsOrdered, frequencyThreshold);
		int newNum = trees.get(trees.size() - 1).getTotalCount();
		boolean result = newNum >= frequencyThreshold;

		boolean oldVal = false;// calc as defined above?
		if (result != oldVal) {
			System.err.println("err");
		}
		return result;
	}

	private static List<FpTreeForMcmc> tmpTrees = new ArrayList<FpTreeForMcmc>();

	private List<FpTreeForMcmc> getConditionalTrees(int[] itemsOrdered, int threshold) {
		int i = 0;
		while (itemsOrdered.length > i && tmpTrees.size() > i
				&& (itemsOrdered[i] == tmpTrees.get(i).conditionedOn.get(i))) {
			i++;
		}

		FpTreeForMcmc startTree;
		if (i == 0) {
			// System.out.println("using zero");
			startTree = fpTrees[itemsOrdered[0]];
			tmpTrees = new ArrayList<FpTreeForMcmc>();
			tmpTrees.add(startTree);

			i = 1;
		} else {
			// System.out.println("using " + i);
			ArrayList<FpTreeForMcmc> tmpTrees2 = new ArrayList<FpTreeForMcmc>();
			startTree = tmpTrees.get(0);

			for (int j = 0; j < i; j++) {
				startTree = tmpTrees.get(j);
				tmpTrees2.add(startTree);
			}

			tmpTrees = tmpTrees2;
		}

		FpTreeForMcmc nextTree = startTree;
		for (; i < itemsOrdered.length; i++) {
			int item = itemsOrdered[i];
			nextTree = nextTree.getConditionalFPTree(item, threshold);
			tmpTrees.add(nextTree);
		}

		return tmpTrees;
	}

	public FpTreeForMcmc getTree(int index) {
		if (fpTrees != null && fpTrees[index] != null)
			return fpTrees[index];

		return uncondTree.getConditionalFPTree(index, threshold);
	}

	public FpTreeForMcmc getUnconditionedTree() {
		return uncondTree;
	}
}
