package be.kuleuven.flexics.xps

import java.io.{BufferedWriter, File, FileWriter}

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.cp4im._
import be.kuleuven.flexics.eflexics.Eclat

object Cp4imDatasetStatistics extends App {
  private val (countItemsets, storeItemsets) = args match {
    case Array(ci, si) => (ci == "1", ci == "1" && si == "1")
    case _ => (false, false)
  }

  private val patternsDir = new File(System.getProperty("flexics.xps.patternsDir"))
  if (storeItemsets) patternsDir.mkdirs()

  private val weightNames = Array("uniform", "frequency", "purity")

  print("name;items;size;density;minsup;minlen")
  println(if (countItemsets) ";f;fcl" else "")

  minsups.keys.toSeq.sorted.foreach { datasetName =>
    print(s"$datasetName;")

    val dataset = loadDataset(s"$datasetName.cp4im")
    print(f"${dataset.attributes.size}%d;${dataset.size}%d;${density(dataset)}%.1f")

    val minsup = minsups(datasetName)
    val minlen = minlens(datasetName)
    print(f";${minsup}_${minsup.toDouble / dataset.size}%.3f;${minlen.abs}%s")

    val (weights, outputF, outputFCL) = if (storeItemsets)
      (Array({ _: Itemset => 1.0 }, { p: Itemset => p.size.toDouble }, purity(datasetName)),
        new File(patternsDir, s"$datasetName-f.txt"),
        new File(patternsDir, s"$datasetName-fcl.txt"))
    else
      (null, null, null)

    def writeItemsetToFile(itemset: Itemset, file: BufferedWriter): Unit = {
      file.write(itemset.items.mkString("+"))
      file.write(';')
      file.write(weights.map(_(itemset)).mkString(";"))
      file.newLine()
    }

    if (countItemsets) {
      var f = 0
      var file = if (storeItemsets) new BufferedWriter(new FileWriter(outputF)) else null
      Eclat.mine(dataset, minsup) { p =>
        f += 1
        if (storeItemsets) writeItemsetToFile(p, file)
      }
      if (storeItemsets) { file.flush(); file.close() }
      print(f";$f%d")

      if (minlen != 0) {
        file = new BufferedWriter(new FileWriter(outputFCL))
        val fcl = CP4IM(dataset, Seq(MinSupport(minsup), Closed, MinLength(minlen.abs)))
          .solve()
          .map { p =>
            if (storeItemsets) writeItemsetToFile(p, file)
            1
          }.sum
        if (storeItemsets) { file.flush(); file.close() }
        println(s";$fcl")
      } else {
        println(";-1")
      }
    } else {
      println()
    }
  }

  private def density(dataset: Dataset[Set[Int]]): Double = {
    val ones = dataset.records.foldLeft(0) { case (sum, t) => sum + t.values.size }
    100.0 * ones / (dataset.attributes.size * dataset.size)
  }
}
