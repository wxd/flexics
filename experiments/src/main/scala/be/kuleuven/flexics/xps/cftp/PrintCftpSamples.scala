package be.kuleuven.flexics.xps.cftp

import be.kuleuven.pmlib.wrappers.cftp.{CFTPSampling, Distribution, UnsupervisedDistribution}
import be.kuleuven.flexics.xps.{loadDataset, parseConstraints}

object PrintCftpSamples extends App {
  private val Array(datasetName, cs, proposalName, samples) = args

  private val dataset = loadDataset(datasetName)
  private val proposal = Distribution(proposalName).asInstanceOf[UnsupervisedDistribution]
  private val constraints = parseConstraints(cs).map(checker)

  private val invDatasetSize = 1.0 / dataset.size
  private var rawSamples = 0
  CFTPSampling.sample(dataset, proposal, computeMasks = true)
    .takeWhile { _ => !System.out.checkError() } // Enables usage in pipes
    .filter { p =>
      rawSamples += 1
      constraints.forall(c => c(p))
    }
    .take(samples.toInt)
    .foreach { p =>
      println(f"${p.sortedItems.mkString("+")}%s;${p.size * invDatasetSize}%f")
    }

  System.err.println(f"$rawSamples%d;$samples%s;${samples.toDouble / rawSamples}%.6e")
}
