package be.kuleuven.flexics.xps.cftp

import be.kuleuven.pmlib.wrappers.cftp.{CFTPSampling, Distribution, UnsupervisedDistribution}
import be.kuleuven.flexics.xps.{loadDataset, parseConstraints, timedIterator}

object TimeCFTP extends App {
  private val Array(datasetName, cs, proposalName, n, measurements) = args

  private val dataset = loadDataset(datasetName)
  private val proposal = Distribution(proposalName).asInstanceOf[UnsupervisedDistribution]
  private val constraints = parseConstraints(cs).map(checker)

  private val samples = n.toInt
  var totalSamples = 0L
  var acceptedSamples = 0L
  @inline def printAcceptanceRate(): Unit =
    System.err.println(f"acceptance+stats;$n%s;$acceptedSamples%d;$totalSamples%d;${acceptedSamples.toDouble / totalSamples}%.6e")
  sys.addShutdownHook { printAcceptanceRate() }

  for (m <- 1 to measurements.toInt) {
    totalSamples = 0L
    acceptedSamples = 0L
    val cftpSampleIterator = CFTPSampling.sample(dataset, proposal, computeMasks = true)
      .filter { p =>
        totalSamples += 1
        constraints.forall(c => c(p))
      }

    val samplingTime = timedIterator(cftpSampleIterator)
      .take(samples)
      .zipWithIndex
      .foldLeft(0.0) { case (totalSamplingTime, ((itemset, sampleTime), i)) =>
        val ms = sampleTime.inNanoseconds * 1e-06

        println(f"$m;${i + 1};$ms%.6f")
        acceptedSamples += 1

        totalSamplingTime + ms
      }

    println(f"$m;-1;${samplingTime / samples.toDouble}%.6f")
    printAcceptanceRate()
  }
}
