package be.kuleuven.flexics.xps

import be.kuleuven.flexics.xps.acfi.{PrintAcfiSamples, TimeACFI}
import be.kuleuven.flexics.xps.cftp.{PrintCftpSamples, TimeCFTP}
import be.kuleuven.flexics.xps.flexics.{PrintEFlexicsSamples, PrintGFlexicsSamples, TimeEFlexics, TimeGFlexics}
import be.kuleuven.flexics.xps.lcm.GenerateAwkScript

object ExperimentDispatcher extends App {
  args match {
    case Array("sample", "eflexics", _*) => PrintEFlexicsSamples.main(experimentArgs)
    case Array("sample", "gflexics", _*) => PrintGFlexicsSamples.main(experimentArgs)
    case Array("time", "eflexics", _*) => TimeEFlexics.main(experimentArgs)
    case Array("time", "gflexics", _*) => TimeGFlexics.main(experimentArgs)
    case Array("sample", "acfi", _*) => PrintAcfiSamples.main(experimentArgs)
    case Array("time", "acfi", _*) => TimeACFI.main(experimentArgs)
    case Array("sample", "cftp", _*) => PrintCftpSamples.main(experimentArgs)
    case Array("time", "cftp", _*) => TimeCFTP.main(experimentArgs)
    case Array("sample", "lcm", _*) => GenerateAwkScript.main(experimentArgs)
    case Array() =>
      System.err.println("Argument list is empty")
      System.exit(1)
  }

  private def experimentArgs: Array[String] = args.drop(2)
}
