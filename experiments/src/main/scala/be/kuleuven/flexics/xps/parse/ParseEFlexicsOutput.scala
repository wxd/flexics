package be.kuleuven.flexics.xps.parse

import java.io.File

import scala.io.Source

object ParseEFlexicsOutput extends App {
  // private val measurements = 5
  private val nSamples = 101

  private val dir = new File("/Users/vladimir/Documents/phd/code/wg4ps/experiments/results")
  private val batches = dir.listFiles().view.filter(_.getName.startsWith("accidents-0.9")).flatMap { f =>
    Source.fromFile(f).getLines().drop(7).grouped(nSamples + 1).map { m =>
      val samples = m.drop(1).map(parse).toIndexedSeq
      samples
    }
  }.take(100).toIndexedSeq

  println(batches.size)

  var counter = Map[Int, Array[Int]]()
  batches.foreach { batch =>
    batch.zipWithIndex.foreach { case (sample, positionInBatch) =>
      sample.items.foreach { item => counter = increment(counter, item, positionInBatch) }
    }
  }

  println(accidentsSortedItems
    .map(accidents3000ItemInItemsetFrequencies(_))
    .map(_ * batches.size)
    .map(expectedCount => f"$expectedCount%.9f")
    .mkString(" "))
  println()


  accidentsSortedItems.foreach { item =>
    val counts = counter.getOrElse(item, Array.fill(nSamples)(0))
    println(counts.mkString(" "))
  }

  private def increment(counter: Map[Int, Array[Int]], item: Int, position: Int) =
    if (counter contains item) {
      counter(item)(position) += 1
      counter
    } else {
      val itemCounts = Array.fill(nSamples)(0)
      itemCounts(position) = 1
      counter.updated(item, itemCounts)
    }

  private final case class Sample(i: Int, items: Set[Int], ms: Int)
  private def parse(line: String): Sample = {
    val Array(i, items, tail) = line.split("[\\{|\\}]")
    Sample(
      i = i.trim.toInt,
      items = items.trim.split(", ").view.map(_.toInt).toSet,
      ms = tail.substring(1).split(" ")(1).toInt
    )
  }
}
