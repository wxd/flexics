package be.kuleuven.flexics

import java.io.File

import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets._
import be.kuleuven.pmlib.labels._
import be.kuleuven.pmlib.labels.binary.{Purity => PurityMeasure}
import be.kuleuven.weightgen.BoundedWeightFunction
import be.kuleuven.cp4im.{Closed, MinLength, MinSupport}
import com.twitter.conversions.time._
import com.twitter.util.Duration

import scala.io.Source

package object xps {
  val DatasetDirProperty = "flexics.xps.datasetDir"
  val DatasetDir = System.getProperty(DatasetDirProperty, "datasets")

  lazy val (minsups, minlens) = Source.fromInputStream(getClass.getResourceAsStream("/parameters.txt"))
    .getLines()
    .drop(1) // Ignore the header line
    .foldLeft((Map[String, Int](), Map[String, Int]())) { case ((minsups0, minlens0), line) =>
      val Array(datasetName, minsup, minlen, fclCount) = line.split(";")
      (minsups0.updated(datasetName, minsup.toInt),
        minlens0.updated(datasetName, if (fclCount == "enough") minlen.toInt else -minlen.toInt))
    }

  final def time[T](function: () => T): (T, Duration) = {
    val startTime = System.nanoTime()
    val result = function.apply()
    val endTime = System.nanoTime()
    (result, (endTime - startTime).nanoseconds)
  }

  final def timedIterator[T](iterator: => Iterator[T]): Iterator[(T, Duration)] = {
    val t = System.nanoTime()
    new Iterator[(T, Duration)] {
      var startTime: Long = t
      var firstCall = true
      val base = iterator

      override def hasNext: Boolean = {
        if (!firstCall)
          startTime = System.nanoTime()
        else
          firstCall = false
        base.hasNext
      }

      override def next(): (T, Duration) = {
        val v = base.next()
        val elapsed = (System.nanoTime() - startTime).nanoseconds
        (v, elapsed)
      }
    }
  }

  def loadDataset(name: String, withLabel: Boolean = false, zeroBased: Boolean = true): Dataset[Set[Int]] = {
    val ds = Option(new File(DatasetDir).listFiles()).flatMap(_.find(_.getName.startsWith(s"$name"))).map { file =>
      val extension = file.getName.substring(file.getName.lastIndexOf('.') + 1)
      extension match {
        case "cp4im" => CP4IMData.load(name = name, path = file, CP4IMParameters(dropLabel = !withLabel))
        case "fimi" => FIMIData.load(name = name, path = file, FIMIParameters(oneBased = !zeroBased))
      }
    }

    if (ds.isEmpty)
      System.err.println(s"Dataset '$name' not found in '$DatasetDir'")

    ds.get
  }

  def parseConstraints(arg: String): Seq[Constraint] =
    arg.toLowerCase.split(',').sorted.reverseIterator.collect {
      case minsup if minsup.startsWith("f") => MinSupport(minsup.drop(1).toInt)
      case minlen if minlen.startsWith("l") => MinLength(minlen.drop(1).toInt)
      case "c1" => Closed
    }.filter {
      case MinSupport(minsup) => minsup > 0
      case MinLength(minlen) => minlen > 0
      case _ => true
    }.toSeq

  def purity(datasetName: String) = {
    val labelledDataset = loadDataset(datasetName, withLabel = true) withBinaryLabel "positive_class"
    val model = PurityMeasure.computeState(labelledDataset)
    new BoundedWeightFunction[Itemset] {
      override def tiltBound: Double = 2
      override def toString(): String = "Purity"

      override def weight(solution: Itemset): Double = {
        val converted = Itemset(solution.items, labelledDataset, Some(solution.mask))
        PurityMeasure.score(converted, labelledDataset, Some(model))
      }
    }
  }
}
