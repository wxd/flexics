package be.kuleuven.flexics.xps.parse

import java.io.File

import scala.io.Source

object ParseAwkOutput extends App {
  private type Counter = Map[Int, Array[Int]]

  private val nSamples = 100
  private val dir = new File("/Users/vladimir/Documents/phd/code/wg4ps/experiments/results")

  private val batches = dir.listFiles().view.filter(_.getName.endsWith(".batches")).map { f =>
    // println(f.getName)
    val lines =  Source.fromFile(f).getLines()
    val start = lines.next().drop(6).toInt
    lines.take(nSamples).foldLeft((0, start, List[Sample]())) { case ((i, start0, samplesAcc), l) =>
      val parts = l.split(" ")
      val timestamp = parts(1).toInt
      val sample = Sample(i, parts(0).toLong, timestamp - start0,
        parts.view.drop(2).takeWhile(!_.startsWith("(")).map(_.toInt).toSet)
      (i + 1, timestamp, sample :: samplesAcc)

    }._3.reverse.toIndexedSeq
  }
  println(batches.size)

  var counter: Counter = Map[Int, Array[Int]]()
  batches.foreach { batch =>
    batch.zipWithIndex.foreach { case (sample, positionInBatch) =>
      sample.items.foreach { item => counter = increment(counter, item, positionInBatch)}
    }
  }

  println(accidentsSortedItems
    .map(accidents3000ItemInItemsetFrequencies(_))
    .map(_ * batches.size)
    .map(expectedCount => f"$expectedCount%.9f")
    .mkString(" "))
  println()


  accidentsSortedItems.foreach { item =>
    val counts = counter.getOrElse(item, Array.fill(nSamples)(0))
    println(counts.mkString(" "))
  }

  private def increment(counter: Counter, item: Int, position: Int) =
    if (counter contains item) {
      counter(item)(position) += 1
      counter
    } else {
      val itemCounts = Array.fill(nSamples)(0)
      itemCounts(position) = 1
      counter.updated(item, itemCounts)
    }

  private final case class Sample(i: Int, lineNumber: Long, seconds: Int, items: Set[Int])
}
