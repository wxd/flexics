package be.kuleuven.flexics.xps

import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.flexics.Constraint
import be.kuleuven.cp4im.{Closed, MinLength, MinSupport}

import scala.annotation.tailrec
import scala.collection.Iterable

package object cftp {
  @inline def checkMinLength(minlen: Int)(itemset: Itemset): Boolean =
    itemset.description.length >= minlen

  @inline def checkMinSupport(minsup: Int)(itemset: Itemset): Boolean =
    itemset.size >= minsup

  def checkClosed(itemset: Itemset): Boolean = {
    val dataset = itemset.dataset
    val items = itemset.items

    @tailrec def checkRec(closure: Set[Int], indicesToCheck: Iterable[Int]): Boolean =
      indicesToCheck.headOption match {
        case Some(i) =>
          val closure0 = closure intersect dataset(i).values
          if (items subsetOf closure0)
            checkRec(closure0, indicesToCheck.tail)
          else
            true
        case None =>
          closure == items
      }

    checkRec(Set.empty[Int], itemset.mask.indices())
  }

  @inline def checker(constraint: Constraint): Itemset => Boolean =
    constraint match {
      case MinSupport(minsup) => checkMinSupport(minsup)
      case MinLength(minlen) => checkMinLength(minlen)
      case Closed => checkClosed
    }
}
