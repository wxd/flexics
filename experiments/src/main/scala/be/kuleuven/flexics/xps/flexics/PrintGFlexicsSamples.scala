package be.kuleuven.flexics.xps.flexics

import be.kuleuven.weightgen.{Uniform, WeightGenConfiguration}
import be.kuleuven.flexics.gflexics.GFlexics
import be.kuleuven.cp4im.{CP4IM, Closed, MinLength, MinSupport}
import be.kuleuven.flexics.xps._
import be.kuleuven.flexics.{GetWeight, Support}

import scala.util.Random

object PrintGFlexicsSamples extends App {
  private val Array(datasetName, cs, w, kappa, samples) = args

  private val dataset = loadDataset(datasetName)
  private val constraints = parseConstraints(cs)

  private val problem = CP4IM(dataset, constraints)
  private val weight = w.toLowerCase match {
    case "uniform"   => Uniform
    case "frequency" => Support(dataset, constraints)
    case "purity"    => purity(datasetName)
  }

  private val wg = GFlexics
  private val wgConf = WeightGenConfiguration(kappa = kappa.toDouble)

  private val seed = System.nanoTime().hashCode()
  private val rnd = new Random(seed)
  System.err.println(seed)

  wg.countThenSample(problem, weight, wgConf, solutionWeight = Some(GetWeight), lazyCounting = false)(rnd)
    .take(samples.toInt)
    .foreach { itemset =>
      println(f"${itemset.sortedItems.mkString("+")}%s;${GetWeight(itemset)}%f")
    }
}
