package be.kuleuven.flexics.xps.flexics

import be.kuleuven.weightgen.{Uniform, WeightGenConfiguration, WeightMcConfiguration}
import be.kuleuven.flexics.eflexics.{EclatProblem, EFlexics}
import be.kuleuven.flexics.xps._
import be.kuleuven.flexics.{Support, _}

import scala.util.Random

object TimeEFlexics extends App {
  private val Array(datasetName, minsup, w, kappa, n, measurements) = args

  private val dataset = loadDataset(datasetName)
  private val problem = EclatProblem(dataset, minsup.toInt)

  private val weight = w.toLowerCase match {
    case "uniform" => Uniform
    case "frequency" => Support(dataset, problem.constraints)
  }

  private val wg = EFlexics
  private val wgConf = WeightGenConfiguration(kappa = kappa.toDouble)

  private val samples = n.toInt
  for (m <- 1 to measurements.toInt) {
    val seed = System.nanoTime().hashCode()
    val rnd = new Random(seed)
    System.err.println(s"Measurement $m, seed = $seed")

    val (wmc, countingTime) = time { () => wg.count(problem, weight, WeightMcConfiguration.default)(rnd) }
    println(s"$m;0;${countingTime.inMilliseconds}")

    val samplingTime = timedIterator(wg.sample(problem, weight, wgConf, wmc, Some(GetWeight))(rnd))
      .take(samples.toInt)
      .zipWithIndex
      .foldLeft(0L) { case (totalSamplingTime, ((itemset, sampleTime), i)) =>
        val ms = sampleTime.inMilliseconds
        println(s"$m;${i + 1};$ms")

        totalSamplingTime + ms
      }
    println(s"$m;-1;${(samplingTime + countingTime.inMilliseconds) / samples.toDouble}")
  }
}
