package be.kuleuven.flexics.xps.lcm

import scala.util.Random

object GenerateAwkScript extends App {
  private val Array(itemsets, samples, print) = args

  private val maxIndex = itemsets.toLong - 1
  private val sampleCount = samples.toInt
  private val printSamples = print == "1"

  private val arrayName = "samples"
  private val seed = System.nanoTime().hashCode().abs
  private val rnd = new Random(seed)

  private val lineIndices = IndexedSeq.fill(sampleCount)((rnd.nextDouble() * maxIndex).toLong)
  private val minSampleIndex = lineIndices.min
  private val maxSampleIndex = lineIndices.max

  println("\tBEGIN {")
  lineIndices.sorted.foreach { li => println(s"\t\t$arrayName[$li]=0") }
  println("\t\tprint \"begin\", systime()")
  println("\t\tfflush()")
  println("\t}")
  println("\t{")
  println(s"\t\tif ($minSampleIndex <= NR && NR <= $maxSampleIndex && NR in $arrayName) {")
  println(s"\t\t\tprint NR, systime()${if (printSamples) ", $0" else ""}")
  println("\t\t\tfflush()")
  println(s"\t\t\tdelete $arrayName[NR]")
  println(s"\t\t}")
  println("\t}")
  println("\tEND { print \"end\", systime(); fflush() }")
}
