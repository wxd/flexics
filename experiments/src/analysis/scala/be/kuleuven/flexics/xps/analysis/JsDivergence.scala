package be.kuleuven.flexics.xps.analysis

import java.io.File

import be.kuleuven.flexics.xps.analysis.{jsDivergence => calculateJSD}

import scala.io.Source

object JsDivergence extends App {
  private val (resultsDir, patternsDir, targetSampleCount, methodFilter) = args match {
    case Array(rd, pd) =>
      (new File(rd), new File(pd), 900000, { _: File => true })
    case Array(rd, pd, tsc) =>
      (new File(rd), new File(pd), tsc.toInt, { _: File => true })
    case Array(rd, pd, tsc, methodName) =>
      (new File(rd), new File(pd), tsc.toInt, { f: File => f.getName.startsWith(s"sample+$methodName") })
  }

  private val targetDistributions = groundTruth(patternsDir)

  files(resultsDir)
    .iterator
    .filter(f => f.getName.startsWith("sample+")
      && (f.getName.endsWith(".out") || f.getName.endsWith(".out~"))
      && methodFilter(f))
    .flatMap(f => Setting.fromFile(f).map(s => (s, f)))
    .toSeq
    .sortBy(_._1)
    .foreach { case (setting, file) =>
      jsDivergence(file, setting) match {
        case Right((jsd, samples)) =>
          println(f"${setting.dataset}%-17s ${setting.weight}%-9s ${setting.method}%-8s ${if (setting.isFcl) "FCL" else "F  "}%s ${setting.params}%-5s $jsd%.3f $samples%6d")
        case Left(samples) =>
          // System.err.println(s"'${file.getName}' only contains $samples samples")
      }
    }


  private final def jsDivergence(sampleFile: File, setting: Setting) = {
    val (sampleCounts, totalSamples) = count(
      Source.fromFile(sampleFile).getLines()
        .filter { l => l.nonEmpty &&
          (setting.method != "acfi" || !l.startsWith("Building FpTree representation|")) &&
          (setting.method != "gflexics" || l.endsWith("ms"))
        }
        .map { l =>
          setting.method match {
            case "gflexics" =>
              val items = l.substring(l.indexOf(')') + 2, l.indexOf(" in ")).split(' ').map(_.toInt).toSet
              items
            case _ =>
              val Array(items, _) = l.split(';')
              items.split('+').filter(_.nonEmpty).map(_.toInt).toSet
          }
        }
        .filter(_.nonEmpty)
    )

    if (totalSamples >= targetSampleCount) {
      val empiricalProbabilities = sampleCounts.map { case (pattern, count) => pattern -> count / totalSamples.toDouble }
      val targetProbabilities = targetDistributions((setting.dataset, setting.isFcl, setting.weight))

      Right((calculateJSD(empiricalProbabilities, targetProbabilities), totalSamples))
    } else {
      Left(totalSamples)
    }
  }
}
