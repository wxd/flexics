package be.kuleuven.flexics.xps.analysis

import scala.io.Source

object CftpAcceptanceRate extends App {
  private val Array(resultsDir) = args

  val resultsGroupedBySetting = files(resultsDir)
    .filter(f => f.getName.startsWith("time+cftp") && f.getName.endsWith(".err"))
    .groupBy(Setting.fromFile)

  resultsGroupedBySetting.keys.toSeq.sorted.foreach {
    case ss@Some(setting) =>
      val files = resultsGroupedBySetting(ss)
      val medianAcceptanceRate = median(files.flatMap { f =>
        Source.fromFile(f).getLines()
          .filter(_.startsWith("acceptance+stats"))
          .map(_.split(';').last.toDouble)
      })

      val logAcceptanceRate =
        if (medianAcceptanceRate <= 0 || medianAcceptanceRate.isNaN)
          '-'
        else
          math.log10(medianAcceptanceRate).round.toString

      println(f"${setting.dataset}%20s ${if (setting.isFcl) '1' else '0'} ${setting.params}%5s: ${100 * medianAcceptanceRate}%5.2f $logAcceptanceRate%3s")
    case None =>
      resultsGroupedBySetting(None).foreach(_.getName)
  }
}
