package be.kuleuven.flexics.xps.analysis

import java.io.File

import scala.annotation.tailrec
import scala.io.Source

object RuntimeOnFimiDatasets extends App {
  private val timeRegex = "([0-9]+):([0-9]{2})\\.([0-9]{2})elapsed".r
  private val Array(resultsDir) = args.map(new File(_))

  private val datasets = Source.fromInputStream(getClass.getResourceAsStream("/parameters-fimi.txt"))
    .getLines()
    .drop(1)
    .map(_.split(';')(0))
    .toSet

  private val settings = files(resultsDir)
    .filter(f => datasets.exists(f.getName.contains) && f.getName.endsWith(".out"))
    .flatMap(f => Setting.fromFile(f).map((_, f)))
    .sortBy(_._1)

  settings.foreach { case (setting, file) =>
    printSetting(setting)

    val (measurements, countingTime, timePerSample) = setting.method match {
      case "eflexics" => parseEflexics(file)
      case "lcm"      => parseLcm(file)
    }

    println(f"$countingTime%6.2f $timePerSample%6.2f $measurements%2d")
  }

  private def printSetting(s: Setting) =
    print(f"${s.dataset}%-12s ${s.constraints.mkString(",")}%-7s ${s.method}%-8s ${s.params}%-3s ")

  private def parseEflexics(f: File) = {
    val times = Source.fromFile(f).getLines()
      .map(_.split(';'))
      .filter(xs => xs(1) != "-1")
      .map(xs => (xs(0), xs(1) == "0", xs(2).toLong))
      .toIndexedSeq
      .groupBy(_._1)
      .values.map { times0 =>
        val countingMin = times0.find(_._2).map(_._3 / (60.0 * 1000)).getOrElse(Double.NaN)
        val (samples, samplingTotalSec) = times0.filterNot(_._2).foldLeft((0, 0.0)) { case ((cnt, time), (_, _, ms)) =>
          (cnt + 1, time + ms / 1000.0)
        }
        (countingMin, samplingTotalSec / samples)
      }
    (times.size, times.view.map(_._1).sum / times.size, times.view.map(_._2).sum / times.size)
  }



  private def parseLcm(f: File) = {
    var measurements: Int = 0
    var countingMinTotal: Double = 0.0
    var samplingSecTotal: Double = 0.0

    @inline def span(lines: Iterator[String]): (Iterator[String], Iterator[String]) =
      lines.drop(1).span(l => !l.startsWith("# Measurement"))

    @tailrec def parseRec(iterator: Iterator[String]): Boolean = {
      val (measurement, rest) = span(iterator)
      if (measurement.nonEmpty) {
        measurements += 1

        val timeRegex(cntMin, cntSecW, cntSecF) = measurement
          .find(_.contains("elapsed"))
          .get
          .split(' ')(2)
        countingMinTotal += cntMin.toDouble + cntSecW.toDouble / 60 + cntSecF.toDouble / (100 * 60)

        var samplesInBatches: Int = 0
        var samplingSecBatches: Double = 0.0

        def processBatch(batchIndex: Int) =
          if (measurement.nonEmpty) {
            val measurement0 = measurement.dropWhile(l => !l.startsWith("## Batch ")).drop(1)

            var start = measurement0.next().split(' ') match { case Array("begin", t) => t.toLong }
            measurement0.takeWhile(l => !l.startsWith("end")).foreach { l =>
              val t = l.split(' ')(1).toLong

              samplingSecBatches += t - start
              samplesInBatches += 1

              start = t
            }
          } else {
            System.err.println(s"No batch $batchIndex")
          }

        processBatch(1)
        processBatch(2)
        samplingSecTotal += samplingSecBatches / samplesInBatches

        parseRec(rest)
      } else {
        false
      }
    }

    parseRec(Source.fromFile(f).getLines())
    (measurements, countingMinTotal / measurements, samplingSecTotal / measurements)
  }
}
