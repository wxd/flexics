package be.kuleuven.flexics.xps.analysis

import java.io.File
import java.lang.{Double => JDouble}

import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.random.RandomGeneratorFactory
import org.apache.commons.math3.util.{Pair => CommonsPair}

import scala.collection.JavaConverters._
import scala.util.Random

object PrintIdealSamples extends App {
  private val Array(datasetName, cs, weight, samples) = args
  private val fcl = cs == "f,c,l"

  private val probabilities = targetDistribution
  private val dt = idealSampler(probabilities)
  for (_ <- 1 to samples.toInt) {
    val itemset = dt.sample()
    println(f"${itemset.toSeq.sorted.mkString("+")}%s;${probabilities(itemset)}%f")
  }

  private def targetDistribution: Map[Set[Int], Double] = {
    val patternsDir = new File(System.getProperty("flexics.xps.patternsDir"))
    groundTruth(patternsDir)((datasetName, fcl, weight))
  }

  private def idealSampler(probabilities: Map[Set[Int], Double]): EnumeratedDistribution[Set[Int]] = {
    val patternList = probabilities.view.toSeq.map { case (itemset, probability) =>
      new CommonsPair(itemset, JDouble.valueOf(probability))
    }.asJava

    val seed = System.nanoTime().hashCode()
    val rnd = RandomGeneratorFactory.createRandomGenerator(new Random(seed).self)
    System.err.println(seed)

    new EnumeratedDistribution[Set[Int]](rnd, patternList)
  }
}
