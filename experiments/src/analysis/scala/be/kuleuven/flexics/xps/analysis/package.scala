package be.kuleuven.flexics.xps

import java.io.File

import be.kuleuven.pmlib.utils.InformationTheory.log2
import org.apache.commons.math3.stat.descriptive.rank.Median

import scala.collection.mutable
import scala.io.Source
import scala.util.Try

package object analysis {
  def files(directory: File): Array[File] =
    Option(directory.listFiles()).getOrElse(Array.empty[File])

  def files(directory: String): Array[File] =
    files(new File(directory))

  def median(numbers: Iterable[Double]): Double =
    new Median().evaluate(numbers.toArray)

  def count[A](objects: TraversableOnce[A]): (Map[A, Int], Int) =
    objects.foldLeft((Map.empty[A, Int], 0)) { case ((counts, total), x) =>
      val newCount = counts.getOrElse(x, 0)
      (counts.updated(x, newCount + 1), total + 1)
    }

  def jsDivergence[A](empirical: Map[A, Double], target: Map[A, Double]): Double =
    0.5 * target.view.map { case (x, pt) =>
      val pe = empirical.getOrElse(x, 0.0)
      val pm = 0.5 * (pe + pt)
      pt * log2(pt) + (if (pe > 0) pe * log2(pe) else 0) - (pt + pe) * log2(pm)
    }.sum

  final case class Setting(goal: String,
                           method: String,
                           dataset: String,
                           minsup: Int,
                           constraints: Seq[String],
                           weight: String,
                           params: String) extends Ordered[Setting] {
    val isFcl: Boolean = constraints.exists(c => c(0) != 'f')

    private def asTuple =
      (goal, weight, isFcl, dataset, method, params)

    override def compare(that: Setting): Int = {
      import scala.math.Ordered.orderingToOrdered
       asTuple compare that.asTuple
    }
  }

  object Setting {
    def fromFileName(fn: String): Option[Setting] = {
      val (goal, method, datasetName, constraintString, params, weight) = fn.replace("~", "")
          .dropRight(4).split('+') match {
        case Array(goal0, method0, dataset0, constraintString0, params0, weight0)
            if method0 == "cftp" || method0.endsWith("flexics") =>
          (goal0, method0, dataset0, constraintString0, params0, weight0)
        case Array(goal0, method0, dataset0, constraintString0, weight0)
          if method0 == "acfi" || method0 == "ideal" || method0 == "lcm" =>
          (goal0, method0, dataset0, constraintString0, "", weight0)
        case _ =>
          System.err.println(fn)
          return None
      }

      val constraints = constraintString.split(',') match {
        case Array(onlyMinsup) if onlyMinsup.forall(Character.isDigit) => Array(s"f$onlyMinsup")
        case cs => cs
      }
      val minsup = Try(constraints(0).replace("f", "").toInt).getOrElse(-1)

      val weight1 = if (weight.startsWith("freq")) "frequency" else weight
      Some(Setting(goal, method, datasetName, minsup, constraints, weight1, params))
    }

    def fromFile(file: File): Option[Setting] =
      fromFileName(file.getName)

    def apply(fileName: String): Option[Setting] =
      fromFileName(fileName)

    def apply(file: File): Option[Setting] =
      fromFileName(file.getName)
  }

  def groundTruth(patternsDir: File): ((String, Boolean, String)) => Map[Set[Int], Double] = {
    val weightNames = Array("uniform", "frequency", "purity")
    def accs = weightNames.view.map { w => w -> (Map[Set[Int], Double](), 0.0) }.toMap

    val m = mutable.Map[(String, Boolean, String), Map[Set[Int], Double]]()
    def loadGroundTruth(setting: (String, Boolean, String)): Map[Set[Int], Double] = {
      val (datasetName, requestedFcl, requestedWeight) = setting

      val groundTruthFile = new File(patternsDir, s"$datasetName-f${if (requestedFcl) "cl" else ""}.txt")
      if (groundTruthFile.exists()) {
        val distributions = Source.fromFile(groundTruthFile)
          .getLines()
          .foldLeft(accs) { case (accs0, l) =>
            val Array(items0, values@_*) = l.split(';')
            val items = items0.split('+').view.filter(_.nonEmpty).map(_.toInt).toSet
            weightNames.view.zipWithIndex.map { case (w, i) =>
              val (itemsets, totalWeight) = accs0(w)
              val weight = values(i).toDouble
              w -> (itemsets.updated(items, weight), totalWeight + weight)
            }.toMap
          }
          .mapValues { case (itemsets, totalWeight) =>
            itemsets.mapValues(_ / totalWeight)
          }

        distributions.foreach { case (w, distr) => m.update(setting.copy(_3 = w), distr) }
        distributions(requestedWeight)
      } else {
        System.err.println(s"Ground truth file doesn't exist: '${groundTruthFile.getCanonicalPath}'")
        null
      }
    }

    m.withDefault(loadGroundTruth)
  }
}
