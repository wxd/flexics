#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
source experiments/scripts/shared.sh ${RESULTS_DIR}

DO_NOT_SOURCE_SHARED=0
TIMEOUT=86400 #24h
experiments/scripts/time-cp4im_data/gflexics.sh ${RESULTS_DIR} ${DATASETS_DIR} ${DO_NOT_SOURCE_SHARED}
experiments/scripts/time-cp4im_data/eflexics.sh ${RESULTS_DIR} ${DATASETS_DIR} ${DO_NOT_SOURCE_SHARED}
experiments/scripts/time-cp4im_data/cftp.sh ${RESULTS_DIR} ${DATASETS_DIR} ${TIMEOUT} ${DO_NOT_SOURCE_SHARED}
experiments/scripts/time-cp4im_data/acfi.sh ${RESULTS_DIR} ${DATASETS_DIR} ${DO_NOT_SOURCE_SHARED}