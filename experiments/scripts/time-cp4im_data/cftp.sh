#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
TIMEOUT=$3

# Run common initialization, unless told not to
if [[ $# -ne 4 || $4 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

MEASUREMENTS=5
SAMPLES=1000

export JAVA_OPTS="-Xmx8G -Dflexics.xps.datasetDir=$DATASETS_DIR"

tail -n +2 experiments/src/main/resources/parameters.txt | while read line
do
    PARAMS=(${line//;/ })
    DS_NAME=${PARAMS[0]}
    MINSUP=${PARAMS[1]}
    MINLEN=${PARAMS[2]}

    ENOUGH_LONG_PATTERNS_IN_DS=${PARAMS[3]}
    CONSTRAINT_SETS='0 1'

    for proposal in freq freq4
    do
        for cl in ${CONSTRAINT_SETS}
        do
            if [[ ${cl} = '1' ]]; then
                CONSTRAINTS="f$MINSUP,c1,l$MINLEN"
            else
                CONSTRAINTS="f$MINSUP"
            fi

            OUTPUT_FILE="time+cftp+$DS_NAME+$CONSTRAINTS+$proposal+frequency"
            echo ${OUTPUT_FILE} 1>&2

            ! gnutimeout --preserve-status --signal=SIGINT ${TIMEOUT} \
                experiments/build/install/experiments/bin/experiments \
                time cftp \
                ${DS_NAME} ${CONSTRAINTS} ${proposal} ${SAMPLES} ${MEASUREMENTS} \
                2> "$RESULTS_DIR/$OUTPUT_FILE.err" | tee "$RESULTS_DIR/$OUTPUT_FILE.out" | grep '\-1'
        done
     done
done
