#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2

# Run common initialization, unless told not to
if [[ $# -ne 3 || $3 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

MEASUREMENTS=5
BURN_IN=100000
SAMPLES=1000

export JAVA_OPTS="-Xmx8G -Dflexics.xps.datasetDir=$DATASETS_DIR"

tail -n +2 experiments/src/main/resources/parameters.txt | while read line
do
    PARAMS=(${line//;/ })
    DS_NAME=${PARAMS[0]}
    MINSUP=${PARAMS[1]}

    OUTPUT_FILE="time+acfi+$DS_NAME+$MINSUP+uniform"
    echo ${OUTPUT_FILE} 1>&2

    experiments/build/install/experiments/bin/experiments \
        time acfi \
        "${DATASETS_DIR}/${DS_NAME}.fimi" ${MINSUP} ${BURN_IN} ${SAMPLES} ${MEASUREMENTS} \
        2> "$RESULTS_DIR/$OUTPUT_FILE.err" | tee "$RESULTS_DIR/$OUTPUT_FILE.out" | grep '\-1'
done
