#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
LCM=$3

# Run common initialization, unless told not to
if [[ $# -ne 4 || $4 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

MEASUREMENTS=5
BATCHES=2
SAMPLES=50
PRINT_SAMPLES=0

generate_awk() {
    experiments/build/install/experiments/bin/experiments \
        sample lcm \
        $1 ${SAMPLES} ${PRINT_SAMPLES}
}

for i in $(seq 1 ${MEASUREMENTS})
    do
    tail -n +2 experiments/src/main/resources/parameters-fimi.txt | while read line
    do
        PARAMS=(${line//;/ })
        DS_NAME=${PARAMS[0]}
        MINSUP=${PARAMS[1]}
        COUNT=${PARAMS[2]}

        DS_PATH="$DATASETS_DIR/$DS_NAME.fimi"

        OUTPUT_FILE_NAME="time+lcm+$DS_NAME+f$MINSUP+uniform"
        OUT_FILE="$RESULTS_DIR/$OUTPUT_FILE_NAME.out"
        ERR_FILE="$RESULTS_DIR/$OUTPUT_FILE_NAME.err"
        AWK_FILE="$RESULTS_DIR/$OUTPUT_FILE_NAME.awk"
        echo ${OUTPUT_FILE_NAME} 1>&2

        if [[ ${i} -eq 1 ]]; then
            > ${OUT_FILE}
            > ${ERR_FILE}
        fi

        echo "# Measurement $i" >> ${OUT_FILE}
        /usr/bin/time --output=${OUT_FILE} --append \
            ${LCM} ${DS_PATH} ${MINSUP} >> ${OUT_FILE} 2>> ${ERR_FILE}
        echo "## Itemset count = $COUNT" >> ${OUT_FILE}

        for batch in $(seq 1 ${BATCHES})
        do
            generate_awk ${COUNT} > ${AWK_FILE}
            echo "## Batch $batch" >> ${OUT_FILE}
            ${LCM} ${DS_PATH} ${MINSUP} /dev/stdout 2>> ${ERR_FILE} | \
                awk -f ${AWK_FILE} >> ${OUT_FILE}

            rm ${AWK_FILE}
        done
    done
done
