#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2

# Run common initialization, unless told not to
if [[ $# -ne 3 || $3 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

MEASUREMENTS=5
SAMPLES=100
KAPPAS="0.9 0.5 0.1"

LOGBACK_CONF=experiments/logback.xml
export JAVA_OPTS="-Xmx31G -Dlogback.configurationFile=$LOGBACK_CONF -Dflexics.xps.datasetDir=$DATASETS_DIR"

tail -n +3 experiments/src/main/resources/parameters-fimi.txt | grep -v 'kosarak' | while read line
do
    PARAMS=(${line//;/ })
    DS_NAME=${PARAMS[0]}
    MINSUP=${PARAMS[1]}

    for kappa in ${KAPPAS}
    do
        OUTPUT_FILE="time+eflexics+$DS_NAME+f$MINSUP+$kappa+uniform"
        echo ${OUTPUT_FILE} 1>&2

        experiments/build/install/experiments/bin/experiments \
            time eflexics \
            ${DS_NAME} ${MINSUP} uniform ${kappa} ${SAMPLES} ${MEASUREMENTS} \
                2> "$RESULTS_DIR/$OUTPUT_FILE.err" | tee "$RESULTS_DIR/$OUTPUT_FILE.out" | grep '\-1'
    done
done