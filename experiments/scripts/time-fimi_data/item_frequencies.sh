#!/usr/bin/env bash
set -euo pipefail

INPUT="${1:-/dev/stdin}"
COUNT_ITEMS_AWK="experiments/scripts/time-fimi_data/item_frequencies.awk"

# cat ${COUNT_ITEMS_AWK}
awk -f ${COUNT_ITEMS_AWK} ${INPUT}