#!/usr/bin/env bash
set -euo pipefail

INPUT="${1:-/dev/stdin}"
DENSITY_AWK="experiments/scripts/time-fimi_data/density.awk"

# cat ${DENSITY_AWK}
awk -f ${DENSITY_AWK} ${INPUT}