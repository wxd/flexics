{
    for (i = 1; i <= NF; i++)
        freq[$i]++;
}
END {
    for (item in freq)
        printf "%s;%d;%.17f\n", item, freq[item], freq[item] / NR;
}
