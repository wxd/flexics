BEGIN {
    transactions = 0;
    item_occurrences = 0;
    min_item = inf;
    max_item = -inf;
}

{
    transactions++;

    for (i = 1; i <= NF; i++) {
        item_occurrences++;

        if ($i > max_item) {
            max_item = $i;
        } else if ($i < min_item) {
            min_item = $i
        }
    }
}

END {
    items = max_item - min_item + 1;
    printf "%d;%d;%d;%d;%.0f\n", min_item, max_item, items, transactions, item_occurrences;

    density = (item_occurrences / transactions) / items;
    printf "%.6f;%.6f\n", density, density * 100;
}
