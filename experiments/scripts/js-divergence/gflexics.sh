#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
SAMPLES=$3

# Run common initialization, unless told not to
if [[ $# -ne 3 || $3 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

echo "Path to Gecode libraries (environment variable 'GECODE') is set to '$GECODE'" 1>&2

export LD_LIBRARY_PATH=${GECODE}
LOGBACK_CONF=experiments/logback.xml
export JAVA_OPTS="-Xmx12G -Dlogback.configurationFile=$LOGBACK_CONF -Dflexics.xps.datasetDir=$DATASETS_DIR -Djava.library.path=$GECODE"

INPUT_PARALLEL="$RESULTS_DIR/gflexics.tmp"
tail -n +2 experiments/src/main/resources/parameters.txt | \
    grep -v 'splice\|mushroom' | \
    awk 'BEGIN { FS=";" }; { printf "%s;f%s,c1,l%s;0.9\n", $1,$2,$3 }' > ${INPUT_PARALLEL}

grep 'vote' experiments/src/main/resources/parameters.txt | \
    awk 'BEGIN { FS=";" }; { printf "%s;f%s,c1,l%s;0.5\n", $1,$2,$3;
                             printf "%s;f%s,c1,l%s;0.1\n", $1,$2,$3; }' >> ${INPUT_PARALLEL}

OUTPUT_FILE_NAME="$RESULTS_DIR/sample+gflexics+{1}+{2}+{3}+{4}"
color parallel --gnu -t --progress --retries 10 --joblog parallel.log --colsep ';' \
    experiments/build/install/experiments/bin/experiments \
        sample gflexics \
        "{1}.cp4im" "{2}" "{4}" "{3}" ${SAMPLES} \
        '>'  "$OUTPUT_FILE_NAME.out" '2>' "$OUTPUT_FILE_NAME.err" \
    :::: ${INPUT_PARALLEL} ::: uniform frequency purity

rm ${INPUT_PARALLEL}
