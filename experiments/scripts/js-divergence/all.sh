#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
source experiments/scripts/shared.sh ${RESULTS_DIR}

SOURCE_SHARED=0
experiments/scripts/js-divergence/jsd-samples-gflexics.sh ${RESULTS_DIR} ${DATASETS_DIR} ${SOURCE_SHARED}
experiments/scripts/js-divergence/jsd-samples-eflexics.sh ${RESULTS_DIR} ${DATASETS_DIR} ${SOURCE_SHARED}
experiments/scripts/js-divergence/jsd-samples-cftp.sh ${RESULTS_DIR} ${DATASETS_DIR} ${SOURCE_SHARED}
experiments/scripts/js-divergence/jsd-samples-acfi.sh ${RESULTS_DIR} ${DATASETS_DIR} ${SOURCE_SHARED}
experiments/scripts/js-divergence/jsd-samples-ideal.sh ${RESULTS_DIR} ${DATASETS_DIR} ${SOURCE_SHARED}
