#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
TIMEOUT=$3
SAMPLES=$4

# Run common initialization, unless told not to
if [[ $# -ne 5 || $5 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

export JAVA_OPTS="-Xmx8G -Dflexics.xps.datasetDir=$DATASETS_DIR"

INPUT_PARALLEL="$RESULTS_DIR/cftp.tmp"
tail -n +2 experiments/src/main/resources/parameters.txt | \
    grep 'lymph\|mushroom\|primary\|soybean\|vote\|zoo'  | \
    awk 'BEGIN { FS=";" }; { printf "%s;f%s\n", $1,$2; printf "%s;f%s,c1,l%s\n", $1,$2,$3 }' > ${INPUT_PARALLEL}
# - The list of datasets is based on the timing experiments
# - `awk` prints two lines per dataset, for two constraint sets (F & FCL)

OUTPUT_FILE_NAME="$RESULTS_DIR/sample+cftp+{2}+{3}+{1}+frequency"
parallel --gnu -t --progress --colsep ';' --timeout ${TIMEOUT} \
    experiments/build/install/experiments/bin/experiments \
        sample cftp \
        "{2}" "{3}" "{1}" ${SAMPLES} \
         '>' "$OUTPUT_FILE_NAME.out" '2>' "$OUTPUT_FILE_NAME.err" \
    ::: freq freq4 :::: ${INPUT_PARALLEL}

rm ${INPUT_PARALLEL}
