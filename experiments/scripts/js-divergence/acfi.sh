#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
SAMPLES=$3

BURN_IN=100000

# Run common initialization, unless told not to
if [[ $# -ne 4 || $4 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

export JAVA_OPTS="-Xmx8G -Dflexics.xps.datasetDir=$DATASETS_DIR"

INPUT_PARALLEL="$RESULTS_DIR/acfi.tmp"
tail -n +2 experiments/src/main/resources/parameters.txt | cut -d';' -f1-2 > ${INPUT_PARALLEL}

parallel -t --progress --colsep ';' -a ${INPUT_PARALLEL} \
    experiments/build/install/experiments/bin/experiments \
        sample acfi \
        "${DATASETS_DIR}/{1}.fimi" "{2}" ${BURN_IN} ${SAMPLES} \
        '>'  "$RESULTS_DIR/sample+acfi+{1}+{2}+uniform.out" \
        '2>' "$RESULTS_DIR/sample+acfi+{1}+{2}+uniform.err"

rm ${INPUT_PARALLEL}
