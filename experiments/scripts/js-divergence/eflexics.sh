#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
DATASETS_DIR=$2
SAMPLES=$3

# Run common initialization, unless told not to
if [[ $# -ne 3 || $3 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

KAPPA="0.9"

LOGBACK_CONF=experiments/logback.xml
export JAVA_OPTS="-Xmx4G -Dlogback.configurationFile=$LOGBACK_CONF -Dflexics.xps.datasetDir=$DATASETS_DIR"

INPUT_PARALLEL="$RESULTS_DIR/eflexics.tmp"
tail -n +2 experiments/src/main/resources/parameters.txt | \
    grep -v 'splice' | \
    cut -d';' -f1-2 > ${INPUT_PARALLEL}

color parallel --gnu -v --progress --colsep ';' --retries 2 \
    experiments/build/install/experiments/bin/experiments \
        sample eflexics \
        "{2}.cp4im" "{3}" "{1}" ${KAPPA} ${SAMPLES} \
        '>'  "$RESULTS_DIR/sample+eflexics+{2}+f{3}+$KAPPA+{1}.out" \
        '2>' "$RESULTS_DIR/sample+eflexics+{2}+f{3}+$KAPPA+{1}.err" \
    ::: uniform purity frequency :::: ${INPUT_PARALLEL}

rm ${INPUT_PARALLEL}

VOTE_MINSUP=$(grep 'vote' experiments/src/main/resources/parameters.txt | cut -d';' -f2)
color parallel --gnu -v --progress --colsep ';' --retries 2 \
    experiments/build/install/experiments/bin/experiments \
        sample eflexics \
        "vote.cp4im" ${VOTE_MINSUP} "{1}" "{2}" ${SAMPLES} \
        '>'  "$RESULTS_DIR/sample+eflexics+vote+f$VOTE_MINSUP+{2}+{1}.out" \
        '2>' "$RESULTS_DIR/sample+eflexics+vote+f$VOTE_MINSUP+{2}+{1}.err" \
    ::: uniform purity frequency ::: 0.1 0.5
