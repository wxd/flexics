#!/usr/bin/env bash
set -euo pipefail

RESULTS_DIR=$1
SAMPLES=$2

# Run common initialization, unless told not to
if [[ $# -ne 3 || $3 -ne 0 ]]; then
    source experiments/scripts/shared.sh
fi

OUTPUT_FILE_NAME="$RESULTS_DIR/sample+ideal+{1}+{2}+{3}"
tail -n +2 experiments/src/main/resources/parameters.txt | \
    cut -d';' -f1 | \
    parallel --gnu -t --progress --colsep ';' \
        ./gradlew --no-daemon --no-rebuild --offline --quiet :experiments:idealSampler \
            "-Pargs='{1};{2};{3};$SAMPLES'" \
            '>' "$OUTPUT_FILE_NAME.out" '2>' "$OUTPUT_FILE_NAME.err" \
        :::: - ::: f "f,c,l" ::: uniform frequency purity
