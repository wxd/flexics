#!/usr/bin/env bash
set -euo pipefail

git reset --hard > /dev/null
git rev-parse --short HEAD 1>&2
echo "Building..." 1>&2
./gradlew --quiet :experiments:installDist && ./gradlew --quiet :experiments:analysisClasses && echo "Done building" 1>&2

RESULTS_DIR=$1
mkdir -p $1 > /dev/null

gnutimeout() {
    if hash timeout 2>/dev/null; then
        timeout "$@"
    elif hash gtimeout 2>/dev/null; then
        gtimeout "$@"
    else
        echo "GNU 'timeout' is not installed: individual commands will not time out" 1>&2
        shift 3
        echo "'$@' will be executed" 1>&2
        "$@"
    fi
}

reverse() {
    if hash tac 2>/dev/null; then
        tac
    elif hash gtac 2>/dev/null; then
        gtac
    else
        echo "GNU 'tac' is not installed: 'cat' is used, the dataset order is not reversed" 1>&2
        cat
    fi
}

color() {
    "$@" 2> >(while read line; do echo -e "\e[01;31m$line\e[0m" >&2; done)
}