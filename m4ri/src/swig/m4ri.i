%module m4ri_jni
%{
#include "../../m4ri/m4ri/m4ri.h"	
%}

mzd_t *mzd_init(int const r, int const c);
void mzd_free(mzd_t *A);
void mzd_copy(mzd_t *dest, mzd_t *src);
int mzd_read_bit(mzd_t *m, int r, int c);
void mzd_write_bit(mzd_t *m, int r, int c, int val);
void mzd_echelonize(mzd_t *m, int full);
mzd_t* mzd_init_window	(mzd_t *M, int lowr, int lowc, int highr, int highc);
void mzd_free_window(mzd_t *A);
void mzd_randomize (mzd_t *M);
void mzd_row_swap(mzd_t *M, int rowa, int rowb);
mzd_t* mzd_submatrix(mzd_t *S, mzd_t *M, int lowr, int lowc, int highr, int highc);
int mzd_equal(mzd_t *A, mzd_t *B);
int mzd_echelonize_pluq(mzd_t *A, int full);
int mzd_echelonize_m4ri(mzd_t *A, int full, int k);
void mzd_clear_bits(mzd_t *M, int x, int y, int n);
static const int m4ri_radix;
int mzd_equal(mzd_t *A, mzd_t *B);