#! /bin/sh
# Requires environment variables: INCLUDE_JAVA, INCLUDE_JAVA_OS, LIB_EXTENSION
 
set -euo pipefail

cd generated
gcc -c m4ri_wrap.c \
	-I$INCLUDE_JAVA -I$INCLUDE_JAVA_OS -I../../m4ri/
gcc -dynamiclib ../../m4ri/m4ri/*.o m4ri_wrap.o -o "libm4ri.$LIB_EXTENSION"
