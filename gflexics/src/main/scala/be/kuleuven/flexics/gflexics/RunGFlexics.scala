package be.kuleuven.flexics.gflexics

import be.kuleuven.pmlib.itemsets.{CP4IMData, CP4IMParameters}
import be.kuleuven.pmlib.labels._
import be.kuleuven.weightgen.{WeightGenConfiguration, WeightMcConfiguration}
import be.kuleuven.flexics._
import be.kuleuven.cp4im.{CP4IM, Closed, MinLength, MinSupport}

import scala.util.Random

object RunGFlexics extends App {
  System.loadLibrary("cp4im")

  private val datasetPath = "/Users/vladimir/Documents/phd/code/_bench/cp_sampling/resources/datasets/zoo-1.txt"
  private implicit val dataset = CP4IMData.load(datasetPath, CP4IMParameters(dropLabel = false)) withBinaryLabel "positive_class"

  private val constraints = Seq(MinSupport(0.1), MinLength(5), Closed)
  private val problem = CP4IM(dataset, constraints)

  private val weight = Purity(dataset, constraints)

  private val rnd = new Random(1)
  private val mcConf = WeightMcConfiguration.default
  private val genConf = WeightGenConfiguration(kappa = 0.9)

  GFlexics.countThenSample(problem, weight, genConf, solutionWeight = Some(GetWeight))(rnd).take(1).foreach(println)

  val (totalWeight, totalCount) = problem.solve().foldLeft((0.0, 0)) { case ((weightSum, count), itemset) =>
    // println(itemset)
    (weightSum + weight(itemset), count + 1)
  }
  println(totalWeight, totalCount)
}
