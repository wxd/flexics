package be.kuleuven.flexics.gflexics

import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.weightgen._
import be.kuleuven.weightgen.leapfrogging.{LeapfroggingWeightGen, LeapfroggingWeightMC}
import be.kuleuven.weightgen.logging.{BasicSlf4jWeightGenLogger, BasicSlf4jWeightMcLogger}
import be.kuleuven.cp4im.CP4IM

object WeightMc4IM extends LeapfroggingWeightMC[Itemset, CP4IM] {
  override protected def defaultCounter(problem: CP4IM) = Cp4imBoundedOracle
  override protected val logger = new BasicSlf4jWeightMcLogger("CP4IM-WeightMC")

  @inline
  override protected def iterationsBeforeLeapfrogging(p: CP4IM,
                                                      w: WeightFunction[Itemset],
                                                      conf: WeightMcConfiguration) =
    3
}

object WeightGen4IM extends LeapfroggingWeightGen[Itemset, CP4IM] {
  override protected def defaultSolver(problem: CP4IM) = Cp4imBoundedOracle
  override protected val logger = new BasicSlf4jWeightGenLogger("CP4IM-WeightGen")

  @inline
  override protected def initialExtraXors(p: CP4IM,
                                          w: WeightFunction[Itemset],
                                          lt: Double,
                                          ht: Double,
                                          xors0: Int) =
    1
}

object GFlexics extends WeightGenWrapper[Itemset, CP4IM] {
  override protected val counter = WeightMc4IM
  override protected val sampler = WeightGen4IM

  override def toString = "GFlexics"
}