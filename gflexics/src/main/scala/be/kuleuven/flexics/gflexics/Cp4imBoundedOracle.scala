package be.kuleuven.flexics.gflexics

import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.weightgen.ParityConstraint
import be.kuleuven.weightgen.oracle.{IteratorBasedBoundedOracle, ListBufferBoundedOracle}
import be.kuleuven.flexics
import be.kuleuven.cp4im.{CP4IM, CP4IMIterator}

object Cp4imBoundedOracle extends IteratorBasedBoundedOracle[Itemset, CP4IM] with ListBufferBoundedOracle[Itemset, CP4IM]{
  override protected def storeSolutionWeight(solution: Itemset, weight: Double): Itemset =
      flexics.storeWeightInPlace(solution, weight) // Mutates `solution`!

  override type I = CP4IMIterator
  override protected def solutionIterator(problem: CP4IM,
                                          xors: IndexedSeq[ParityConstraint],
                                          newXor: Option[ParityConstraint]) =
    xors match {
      case IndexedSeq()    => problem.solve()
      case IndexedSeq(xor) => problem.withExtraXOR(xor).solve()
      case _               => problem.withExtraXORs(xors).solve()
    }

  override protected def closeIterator(iterator: CP4IMIterator): Unit =
    iterator.terminate()
}
