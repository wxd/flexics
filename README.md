Implementation of the **Flexics** pattern sampler.

For running the code and reproducing experiments, 
use the Bash scripts in `experiments/scripts`.

For a detailed description, consult 
the following [publication](https://arxiv.org/abs/1610.09263v1):

Dzyuba, V., van Leeuwen, M., & De Raedt, L. (2016). 
*Flexible constrained sampling with guarantees for pattern mining*. 
arXiv:1610.09263.
